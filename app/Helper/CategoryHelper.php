<?php 
use App\Models\Category;

if (!function_exists('get_category_tree')) {
    /**
     * Get Tree Representation of the category
     *
     * @return array
     */
    function get_category_tree()
    {
        $createTree = function($datas, $parent=0,$depth =0) use(&$createTree){
    		$num = count($datas);
	    	if ($num===0 || $depth > 100) {
	    		return [];
	    	}
	    	$collection = [];
	    	for ($i=0;$i<$num;$i++){
	    		if ($parent==$datas[$i]['parent_id']){
	    			$datas[$i]['child']=$createTree($datas,$datas[$i]['id'],$depth+1);
	    			array_push($collection, $datas[$i]);
	    		}
	    	}
	    	return $collection;	
    	};
    	return $createTree(Category::all()->toArray());
    }
}

if (!function_exists('render_category_sidebar')) {
    /**
     * Get Tree Representation of the category in sidebar (html form)
     *
     * @return array
     */
    function render_category_sidebar($categories, $depth=0){
    	$cathtml = "<ul class='nav child_menu'>";
		foreach ($categories as $category) {
			if (!($category['title']=='UNCATEGORIZED' && count($category['child'])==0)){
				$cathtml .= "<li>";
				$cathtml .= "<a href='".route('category',['id'=>$category['id']])."'>".$category['title']."</a>";
				if (count($category['child'])>0){
					$cathtml .= "<a class='tree-nav'><span class='fa fa-chevron-down'></span></a>";
					$cathtml .= render_category_sidebar($category['child'],$depth+1);
				};
				$cathtml .= "</li>";
			}
		};
		if ($depth==0 && \Auth::user()->hasPermissionTo('edit produk-kategori')){
			$cathtml .= "<li class='nav-btn'><a href='". url('/category/0/add'). "'><i class='fa fa-plus'></i> Add New Category</a></li>";
		};
		$cathtml .= "</ul>";
		return $cathtml;
    }
}