<?php
if (!function_exists('trim_content')) {
    /**
     * @param mixed
     */
    function trim_content($strg, $n = 500, $end_char = '&#8230;')
    {
        $str = strip_tags($strg);
        if (strlen($str) < $n){
            return $str;
        };
        $str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));
        if (strlen($str) <= $n){
            return $str;
        };
        $out = "";
        foreach (explode(' ', trim($str)) as $val){
            $out .= $val.' ';
            if (strlen($out) >= $n){
                $out = trim($out);
                return (strlen($out) == strlen($str)) ? $out : $out.$end_char;
            }
        }
     }
}
if (!function_exists('slugify')) {
    function slugify($text,$length=25)
    {
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        $text = trim($text, '-');
        $text = preg_replace('~-+~', '-', $text);
        $text = strtolower($text);
        if (empty($text)) {
            return 'n-a';
        };
        return trim_content($text,$length,'-');
    }
}