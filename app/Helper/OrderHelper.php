<?php
if (!function_exists('payment_statuses')) {
    /**
     * @param mixed
     */
    function payment_statuses(){
        return \DB::table('order_statuses')->where('identity','payment')->get()
            ->mapWithKeys(function($item,$key){
                return [$item->id => $item->status];
            });
    }
}

if (!function_exists('order_statuses')) {
    /**
     * @param mixed
     */
    function order_statuses(){
        return \DB::table('order_statuses')->where('identity','status')->get()
            ->mapWithKeys(function($item,$key){
                return [$item->id => $item->status];
            });
    }
}

if (!function_exists('get_offline_grid')) {
    /**
     * @param mixed
     */
    function get_offline_grid(){
        return \App\Models\Level::find(\DB::table('settings')->where('setting','offline_grid')->first()->value);
    }
}

if (!function_exists('set_offline_grid')) {
    /**
     * @param mixed
     */
    function set_offline_grid($value){
        return \DB::table('settings')->where('setting','offline_grid')->update(['value'=>$value]);
    }
}
if (!function_exists('money_short')){
    function money_short($number){
        if ($number < 1000) {
            return number_format($number);
        } elseif ($number < 1000000) {
            return number_format($number / 1000) . 'rb';
        } else if ($number < 1000000000) {
            return number_format($number / 1000000,1,',','.') . 'jt';
        } else {
            return number_format($number / 1000000000,1,',','.') . 'M';
        };
    }
}