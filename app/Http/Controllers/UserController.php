<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\DataTables\AdminDataTable;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Show all the Admin user.
     *
     * @return \Illuminate\Http\Response
     */
    public function allAdmin(AdminDataTable $datatable) {
        if (\Auth::user()->hasAnyPermission(['edit admin','view admin'])){
            return $datatable->render('pages.useradmin');
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * Show the add profile page.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewAdd()
    {
        if (\Auth::user()->hasPermissionTo('edit admin')) {
            return view('pages.adduser');
        }
        else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * Show the edit profile page.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewEdit($id)
    {
        if (\Auth::user()->hasPermissionTo('edit admin') || (\Auth::user()->id == $id)) {
            return view('pages.edituser',['user'=>\App\Models\User::find($id)]);
        }
        else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * add profile page.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        if (\Auth::user()->hasPermissionTo('edit admin')) {
            $user = new \App\Models\User;
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->phone = $request->get('phone');
            $user->is_admin = true;
            $user->password = bcrypt($request->get('password'));
            if ($user->save()){
                if ($request->has('role')) {
                    $user->syncRoles([$request->get('role')]);
                }
                return redirect()->route('home')->withSuccess('Penambahan Berhasil')->with("text","Admin baru berhasil ditambahkan.");
            }
            else {
                return redirect()->back()->withWarning("Penambahan Gagal")->with("text","Terjadi kesalahan pada sistem, coba lagi atau periksa data yang anda masukkan.");
            }
        }
        else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * edit profile page.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        if (\Auth::user()->hasPermissionTo('edit admin') || (\Auth::user()->id == $id)) {
            $user = \App\Models\User::find($request->get('current_id'));
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->phone = $request->get('phone');
            if ($request->has('password')||$request->get('password')!=''){
                $user->password = bcrypt($request->get('password'));
            };
            if ($user->save()){
                if ($request->has('role')) {
                    $user->syncRoles([$request->get('role')]);
                }
                return redirect()->back()->withSuccess('Update Profil Berhasil')->with("text","profil admin berhasil diubah.");
            }
            else {
                return redirect()->back()->withWarning("Update gagal")->with("text","Terjadi kesalahan pada sistem, coba lagi atau periksa data yang anda masukkan.");
            }
        }
        else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * Delete admin.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request) {
        if (\Auth::user()->hasPermissionTo('edit admin')) {
            $admin = \App\Models\User::find($request->get('current-id'));
            $admin->syncRoles([]);
            if ($admin->delete()){
                return redirect()->route('home')->withSuccess("Admin dihapus")->with('text',"Admin berhasil dihapus");
            }
            else {
                return redirect()->back()->withWarning("Gagal Menghapus Admin")->with('text',"Terjadi kesalahan pada sistem, silahkan dicoba kembali.");
            }
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * Show the change password page
     *
     * @return \Illuminate\Http\Response
     */
    public function viewChangePass()
    {
        return view('pages.changepass');
    }

    /**
     * change the password
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function changePass(Request $request){
 
        if (!(\Hash::check($request->get('current-password'), \Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->withWarning("Terjadi Kesalahan")->with("text","Password lama anda salah. Silahkan coba kembali");
        }
 
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->withWarning("Terjadi Kesalahan")->with("text","Password baru tidak boleh sama dengan password lama.");
        }
        
        $rules = array(
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        );
        
        $validator = \Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return redirect()->back()->withWarning("Terjadi Kesalahan")->with("text",$messages);
        }

        $user = \Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
 
        return redirect()->route('home')->withSuccess("Ubah Password Sukses !")->with("text","Password anda telah diganti.");
 
    }
    
    /**
     * Show all the role admin user.
     *
     * @return \Illuminate\Http\Response
     */
    public function allAdminRole() {
        if (\Auth::user()->hasPermissionTo('view peran')) {
            $data = [
                "roles" => Role::all(),
                "pre_perms" => config('permission.pre_permission')
            ];
            return view('pages.roleadmin',$data);
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * add new admin role.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function addRole(Request $request)
    {
        if (\Auth::user()->hasPermissionTo('edit peran')) {
            $role = Role::create(['name' => $request->get('role_name')]);
            try{
                $role->syncPermissions($request->get('permissions'));
                return redirect()->back()->withSuccess("Peran Ditambahkan")->with("text","Peran baru telah berhasil ditambahkan.");
            } catch(\Exception $e) {
                return redirect()->back()->withWarning("Terjadi kesalahan")->with("text","Silahkan coba beberapa saat lagi, atau silahkan periksa kembali data yg dimasukkan.");
            }
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * edit an admin role.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function editRole(Request $request)
    {
        if (\Auth::user()->hasPermissionTo('edit peran')) {
            $role = Role::findById($request->get('role_id'));
            try{
                $role->syncPermissions($request->get('permissions'));
                return response()->json(['type' => 'success', 'title'=>'Peran diperbarui',"text"=>"Peran telah berhasil diperbarui."]);
            } catch(\Exception $e) {
                return response()->json(['type' => 'danger', 'title'=>'Terjadi Error',"text"=>"Silahkan coba beberapa saat lagi, atau silahkan periksa kembali data yg dimasukkan."]);
            }
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }
    
    /**
     * delete admin role.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function deleteRole(Request $request)
    {
        if (\Auth::user()->hasPermissionTo('edit peran')) {
            $role = Role::findById($request->get('current_id'));
            
            if ($role->delete()){
                return redirect()->back()->withSuccess("Peran dihapus")->with('text',"Peran berhasil dihapus");
            } else {
                return redirect()->back()->withWarning("Terjadi Kesalahan")->with('text',"Refresh halaman ini, lalu silahkan coba beberapa saat lagi");
            }
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }
}
