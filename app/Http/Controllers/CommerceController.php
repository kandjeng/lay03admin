<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Requests;
use JWTAuth;
use Response;
use App\Repository\Transformers\CommerceTransformer;
use \Illuminate\Http\Response as Res;
use Validator;
use Tymon\JWTAuth\Exceptions\JWTException;

class CommerceController extends ApiController
{
    /**
     * @var \App\Repository\Transformers\CommerceTransformer
     * */
    protected $commerceTransformer;

    public function __construct(CommerceTransformer $commerceTransformer)
    {
        $this->commerceTransformer = $commerceTransformer;
    }

    /**
     * get all top level category
     * 
     * @param $id
     * @return json response
     */
    public function top($id){
        if ($id!=1){
            return [
                'status' => 'success',
                'status_code' => $this->getStatusCode(),
                'message' => 'Kategori diambil.',
                "category" => Category::where('id','!=',1)->where('parent_id',$id)->get()->map(function($item,$key){
                    return $this->commerceTransformer->transformCat($item);
                })
            ];
        } 
        return [
            'status' => 'error',
            'status_code' => $this->getStatusCode(),
            'message' => 'Kategori gagal diambil.',
            'category' => []
        ];
    }
    
    /**
     * get all product on category
     * 
     * @param $id
     * @return json response
     */
    public function catProduct(Request $request){
        $cat = Category::find($request->get('cat_id'));
        $prods = collect([]);
        if ($user=JWTAuth::setToken($request->header('X-Auth-Token'))->toUser()){
            $level = $user->customer->level;
            $cat->products->map(function($item,$key)use($level,&$prods){
                if ($item->is_active){
                    $prods->push($this->commerceTransformer->transformMarkup($item,$level));
                }
            });
            return [
                'status' => 'success',
                'status_code' => $this->getStatusCode(),
                'message' => 'Kategori produk diambil.',
                'category' => $cat->title,
                'childCategory' => $cat->children->map(function($item,$key){return $this->commerceTransformer->transformCat($item);}),
                'products' => $prods
            ];
        };
        return [
            'status' => 'error',
            'status_code' => $this->getStatusCode(),
            'message' => 'Kategori produk gagal diambil.',
            'category' => "",
            'childCategory' => [],
            'products' => []
        ];
    }

    /**
     * get all product on category
     * 
     * @param $id
     * @return json response
     */
    public function popProduct(Request $request){
        
        if ($user=JWTAuth::setToken($request->header('X-Auth-Token'))->toUser()){
            $level = $user->customer->level;
            return response()->json([
                'status' => 'success',
                'status_code' => $this->getStatusCode(),
                'message' => 'produk diambil.',
                'products' => Product::select('id','is_active','sku','title','price_base','images','stock')
                		->where('is_active',1)->get()
                		->sortByDesc(function($prod,$key){return $prod->getStock();})->take(10)
                		->map(function($item,$key) use ($level){
                			return $this->commerceTransformer->transformMarkup($item,$level);
                			})->values()
            ]);
        }
        return response()->json([
            'status' => 'error',
            'status_code' => $this->getStatusCode(),
            'message' => 'Produk gagal diambil.',
            'products' =>[]
        ]);
    }
    
    /**
     * get single product
     * 
     * @param $id
     * @return json response
     */
    public function getProduct(Request $request){
        if ($user=JWTAuth::setToken($request->header('X-Auth-Token'))->toUser()){
            if ($prod = Product::find($request->get('product_id'))){
                $level = $user->customer->level;
                $prod->viewed +=1;
                $prod->save();
                return [
                    'status' => 'success',
                    'status_code' => $this->getStatusCode(),
                    'message' => 'produk diambil.',
                    'product' => $this->commerceTransformer->transformProd($prod,$level)
                ];
            }
        }
        return [
            'status' => 'error',
            'status_code' => $this->getStatusCode(),
            'message' => 'Produk gagal diambil.',
            'product' =>[]
        ];
    }

    /**
     * place an order
     * 
     * @param $id
     * @return json response
     */
    public function placeOrder(Request $request){
        try{
            $user = JWTAuth::setToken($request->header('X-Auth-Token'))->toUser();
            $level= $user->customer->level;
            $order = Order::create([
                "status" => 1,
                "payment" => 5,
                "shipping" => 0,
            ]);
            $order->user()->associate($user);
            $order->consignee = $user->name;
            $order->save();
            foreach ($request->orders as $item){
                $product = Product::find($item['product_id']);
                $stocks = json_decode($product->stock,true);
                $left=0;
                foreach ($item['items'] as $good){
                    foreach($stocks as $key=>$stock){
                        if ($good['variance']==$stock['variance']){
                            $stocks[$key]['stock'] -= $good['count'];
                            $left += ($stocks[$key]['stock'] - $stocks[$key]['keep']);
                        } 
                    }
                }
                if (!$left){
                    // $product->is_active = 0;
                };
                $product->stock = json_encode($stocks);
                $product->save();
                $markup = (($level->progress_type=='flat')?$level->price_progress:($level->price_progress*$product->price_base/100));
                \DB::table('order_product')->insert([
                    'order_id' => $order->id,
                    'product_id' => $item['product_id'],
                    'qty' => $item['count'],
                    'sell_price' => ($product->price_base + $markup)*$item['count'],
                    // 'sell_price' => $product->price_base + $markup,
                    'detail'=> json_encode($item['items'])
                ]);
            }
            return [
                "status" => "success",
                "status_code" => $this->getStatusCode(),
                "message" => "Order anda telah kami simpan, silahkan lanjutkan ke pembayaran",
                "order" => $this->commerceTransformer->transformOrder($order)
            ];
        } catch (\Exception $e){
            return [
                "status" => "error",
                "status_code" => $this->getStatusCode(),
                "message" => "terjadi kesalahan ".$e,
                "order" => []
            ];
        }
    }

    /**
     * get order summary user
     * 
     * @param $id
     * @return json response
     */
    public function history(Request $request){
        //try{
            if ($user = JWTAuth::setToken($request->header('X-Auth-Token'))->toUser()){
                $totalDebts = 0;
                $totalOrders = 0;
                $debts = $user->orders->where('payment',7)->sortByDesc('created_at')->map(function ($item,$key)use(&$totalDebts,$user){
                    $totalDebts += ($item->getTotal() + $item->shipping); 
                    return $this->commerceTransformer->transformOrder($item);
                });
                $data = $user->orders->where('status','<',3)->sortByDesc('created_at')->map(function ($item,$key)use(&$totalOrders,$user){
                    $totalOrders += ($item->getTotal() + $item->shipping);
                    return $this->commerceTransformer->transformOrder($item);
                });
                return [
                    "status" => "success",
                    "status_code" => $this->getStatusCode(),
                    "message" => "History berhasil diambil",
                    "totalorders" => $totalOrders,
                    "totaldebts" => $totalDebts,
                    "orders" => $data->values(),
                    "debts" => $debts
                ];
            }
        
    }

    /**
     * finishing order
     * 
     * @param $id
     * @return json response
     */
    public function finishOrder(Request $request){
        try{
            if ($user = JWTAuth::setToken($request->header('X-Auth-Token'))->toUser()){
                $order = Order::find($request->get('order_id'));
                $order->status = 3;
                $order->save();
                return [
                    "status" => "success",
                    "status_code" => $this->getStatusCode(),
                    "message" => "transaksi telah selesai",
                ];
            };
        } catch (\Exception $e){
            return [
                "status" => "error",
                "status_code" => $this->getStatusCode(),
                "message" => "terjadi kesalahan ".$e,
            ];
        }
    }

    /**
     * @description: view all products
     * 
     * @param: null
     * @return: Json String response
     */
    public function products(Request $request){
        try {
            $limit = $request->get('limit') ?: 10;
            if ($user=JWTAuth::setToken($request->header('X-Auth-Token'))->toUser()){
                $level = $user->customer->level;
                $products = Product::where('is_active',1)->orderByDesc('id')->paginate($limit);
                return $this->respondWithPagination($products, [
                    'products' => $this->commerceTransformer->transformProducts($products->all(),$level)
                    ], 'Records Found!');
            };
        } catch (\Exception $e){
            return [
                "status" => "error",
                "status_code" => $this->getStatusCode(),
                "message" => "terjadi kesalahan ".$e,
                "data" => []
            ];
        }
    }

     /**
     * @description: search products by keywords
     * 
     * @param: null
     * @return: Json String response
     */
    public function search(Request $request){
        try {
            $limit = $request->get('limit') ?: 10;
            if ($user=JWTAuth::setToken($request->header('X-Auth-Token'))->toUser()){
                $level = $user->customer->level;
                $search = $request->get('keyword');
                $products = Product::select('id','sku','title','price_base','stock','images')
                    ->where('is_active',1)
                    ->where(function($query)use($search){
                        $query->where('sku','LIKE','%'.$search.'%')
                        ->orWhere('title','LIKE','%'.$search.'%');
                    })->paginate($limit);
                    
                return $this->respondWithPagination($products, [
                    'products' => $this->commerceTransformer->transformProducts($products->all(),$level)
                    ], 'Records Found!');
            };
        } catch (\Exception $e){
            return [
                "status" => "error",
                "status_code" => $this->getStatusCode(),
                "message" => "terjadi kesalahan ".$e,
                "data" => []
            ];
        }
    }
}