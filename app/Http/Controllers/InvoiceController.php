<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Models\Order;
use App\DataTables\InvoiceDataTable;

class InvoiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * View Single Invoice.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(InvoiceDataTable $datatable) {
        if (\Auth::user()->hasPermissionTo('view order')) {
            return $datatable->render('pages.invoice.index');
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }; 
    }

    /**
     * View Single Invoice.
     *
     * @return \Illuminate\Http\Response
     */
    public function view(Invoice $invoice) {
        if (\Auth::user()->hasPermissionTo('view order')) {
            if ($invoice->order->user->is_admin){
                $grid = get_offline_grid();
            } else {
                $grid = $invoice->order->user->customer->level;
            }
            return view('pages.invoice.invoice',["invoice"=>$invoice,"order"=>$invoice->order,'grid'=>$grid]);
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }; 
    }

    /**
     * View Edit Invoice Page.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice) {
        if (\Auth::user()->hasPermissionTo('view order')) {
            return view('pages.invoice.edit',["invoice"=>$invoice,"order"=>$invoice->order]);
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }; 
    }

    /**
     * add an Invoice from order.
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request) {
        if (\Auth::user()->hasPermissionTo('edit order')) {
            if (!Invoice::where('order_id',$request->get('current_id'))->first()){
                try{
                    $invoice = Invoice::create([
                        'order_id'=> $request->get('current_id')
                    ]);    
                    $order = Order::find($request->get('current_id'));
                    $temp = $order->status;
                    $invoice->invoice_number = 'INV/'.$invoice->created_at->format('Ymd').'/'.sprintf("%04d", $invoice->id);
                    $invoice->save();
                    $order->status =3;
                    $order->save();
                    return redirect()->route('viewEditInvoice',['invoice'=>$invoice])->withSuccess('Invoice baru')->with('text',"invoice untuk order sudah dibuat");
                }catch (\Exception $e){
                    $invoice->delete();
                    $order->status = $temp;
                    $order->save();
                    return redirect()->back()->withWarning("Penambahan")->with("text","Terjadi kesalahan saat menambahkan invoice, ulangi beberapa saat lagi.");
                }
            } else {
                return redirect()->route('home')->withWarning("Invoice sudah ada")->with("text","Invoice telah dibuat sebelumnya.");    
            }
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }; 
    }

    /**
     * update an Invoice
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        if (\Auth::user()->hasPermissionTo('set payment')) {
            try {
                $order = Order::find($request->get('current_id'));
                $order->payment = $request->get('payment');
                $invoice = $order->invoice;
                if ($order->getPaymentStatus()=='lunas'||$order->payment==6){
                    $invoice->payment_date = \Carbon\Carbon::now('Asia/Jakarta');
                    $invoice->save();
                }
                $order->save();
                return redirect()->route('invoices')->withSuccess('Invoice disimpan')->with('text',"Perubahan invoice berhasil disimpan");
            } catch (\Exception $e){
                return redirect()->back()->withWarning("Gagal update")->with("text","Terjadi kesalahan saat update, ulangi beberapa saat lagi.");
            }
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }; 
    }
    
    /**
     *  invoice in PDF.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPDF(Invoice $invoice) {
        if (\Auth::user()->hasPermissionTo('view order')) {
            if ($invoice->order->user->is_admin){
                $grid = get_offline_grid();
            } else {
                $grid = $invoice->order->user->customer->level;
            }
            $pdf = \PDF::loadView('pages.invoice.invoice', ["invoice"=>$invoice,"order"=>$invoice->order,'grid'=>$grid])->setPaper('a5');
            return $pdf->download($invoice->invoice_number.'.pdf');
        } else {
            return redirect()->back()->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }; 
    }
}
