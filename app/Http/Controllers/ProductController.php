<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\DataTables\ProductsDataTable;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Show all products.
     *
     * @return \Illuminate\Http\Response
     */
    public function all(ProductsDataTable $datatable) {
        if (\Auth::user()->hasPermissionTo('view produk-kategori')) {
            return $datatable->render('pages.products.products');
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }
    
    /**
     * Show the add product page
     * 
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function viewAdd($id)
    {
        if (\Auth::user()->hasPermissionTo('edit produk-kategori')) {
            $tree = get_category_tree();
            $categoryTree = [];
            //array_push($categoryTree, ['id' => 0, 'title' => 'TOP LEVEL','top' => 'top']);
            $traverse = function($categories,$depth=0) use(&$traverse,&$categoryTree){
                $cat=[];
                foreach ($categories as $category_single) {
                    $cat['id']=$category_single['id'];
                    $cat['title']='';
                    $cat['top']= $depth==0?'first':'';
                    for ($i=0; $i < $depth; $i++) { 
                        $cat['title'].= '&emsp;';
                    };
                    $cat['title'].=$category_single['title'];
                    array_push($categoryTree, $cat);
                    if (count($category_single['child'])>0) {
                        $traverse($category_single['child'],$depth+1);
                    };
                };
            };
            $traverse($tree);
            return view('pages.products.addproduct',['cat_id'=>$id,'category_tree'=>$categoryTree]);
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * Show the edit product page
     * 
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function viewEdit($id)
    {
        if (\Auth::user()->hasPermissionTo('edit produk-kategori')) {
            $tree = get_category_tree();
            $categoryTree = [];
            //array_push($categoryTree, ['id' => 0, 'title' => 'TOP LEVEL','top' => 'top']);
            $traverse = function($categories,$depth=0) use(&$traverse,&$categoryTree){
                $cat=[];
                foreach ($categories as $category_single) {
                    $cat['id']=$category_single['id'];
                    $cat['title']='';
                    $cat['top']= $depth==0?'first':'';
                    for ($i=0; $i < $depth; $i++) { 
                        $cat['title'].= '&emsp;';
                    };
                    $cat['title'].=$category_single['title'];
                    array_push($categoryTree, $cat);
                    if (count($category_single['child'])>0) {
                        $traverse($category_single['child'],$depth+1);
                    };
                };
            };
            $traverse($tree);
            return view('pages.products.editproduct',['category_tree'=>$categoryTree,'product'=>Product::find($id)]);
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * add New product
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request){
        if (\Auth::user()->hasPermissionTo('edit produk-kategori')) {
            //validateinput
            $newProduct = Product::create([
                "category_id" => $request->get('category'),
                "title" => $request->get('title'),
                "sku" => $request->get('sku'),
                "price_base" => $request->get('price_base'),
                "weight" => $request->get('weight'),
                "stock" => json_encode($request->get('stock')),
                "description" => $request->get('description')
            ]);
            try {
                $path = \Storage::disk('public')->getAdapter()->getPathPrefix()."products/".$newProduct->id."/";
                $images = $request->file('image');
                $imgnames = array();
                if (!empty($images)){
                    \File::makeDirectory($path);
                    \File::makeDirectory($path."thumb/");
                    $imgs = array();
                        foreach ($images as $key => $image) {
                        $imgname = slugify($newProduct->title)."_img-".date("YmdHs")."-".$key.".jpg";
                        $imgnames[] = $imgname;
                        $img = \Image::make($image);
                        $img->fit(900)->save($path.$imgname,60);
                        $img->fit(300)->save($path."thumb/".$imgname,60);
                        $imgs[] = "storage/products/".$newProduct->id."/".$imgname;
                    }
                    $newProduct->images = json_encode($imgs);
                    $newProduct->created_by = \Auth::id();
                };
                if ($newProduct->save()){
                    if ($request->get('parent')==1){
                        return redirect()->route('products')->withSuccess("Penambahan berhasil!")->with("text","Produk baru telah berhasil ditambahkan");
                    } else {
                        return redirect()->route('category',["id"=>$request->get('parent')])->withSuccess("Penambahan berhasil!")->with("text","Produk baru telah berhasil ditambahkan");
                    };
                } else {
                    foreach($imgnames as $imag){
                        \File::delete($path.$imag,$path."thumb/".$imag);
                    }
                    return redirect()->back()->withWarning("Penambahan gagal")->with("text","Gagal menambahkan produk, periksa kembali data yang dimasukkan");
                }
             } catch(\Exception $e){
                $path = \Storage::disk('public')->getAdapter()->getPathPrefix()."products/".$newProduct->id."/";
                \File::deleteDirectory($path);
                \File::deleteDirectory($path."thumb/");
                $newProduct->delete();
                return redirect()->route('category',["id"=>$request->get('category')])->withWarning("Penambahan gagal!")->with("text","Terjadi kesalahan saat menambahkan kategori baru"); 
            }
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }
    
    /**
     * delete image from product
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function deleteImg(Request $request){
        if (\Auth::user()->hasPermissionTo('edit produk-kategori')) {
            $product = Product::find($request->get('product_id'));
            $images = json_decode($product->images);
            $imgdel = pathinfo($images[$request->get('img_idx')],PATHINFO_BASENAME);
            $path = \Storage::disk('public')->getAdapter()->getPathPrefix()."products/".$product->id."/";
            try{
                \File::delete($path.$imgdel,$path."thumb/".$imgdel);
                unset($images[$request->get('img_idx')]);
                $product->images = json_encode(array_values($images));
                if ($product->save()){
                    return response()->json(['type' => 'success','title'=>'Gambar dihapus',"text"=>"Gambar telah berhasil dihapus","imgs"=>json_decode($product->images)]);
                } else {
                    return response()->json(['type' => 'danger','title'=>'Gagal menghapus',"text"=>"terjadi kesalahan, coba beberapa saat lagi"]);
                }
            } catch(\Exception $e) {
                return response()->json(['type' => 'danger','title'=>'Gagal menghapus',"text"=>"terjadi kesalahan, coba beberapa saat lagi"]);
            }
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }    
    }

    /**
     * edit product in database
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request){
        if (\Auth::user()->hasPermissionTo('edit produk-kategori')) {
            //validateinput
            $product = Product::find($request->get('current_id'));
            $product->category_id = $request->get('category');
            $product->title = $request->get('title');
            $product->sku = $request->get('sku');
            $product->price_base = $request->get('price_base');
            $product->weight = $request->get('weight');
            $oldstock = json_decode($product->stock,true);
            foreach ($request->get('stock') as $pidx => $newstock){
                $fidx = array_search($newstock['variance'],array_column($oldstock,'variance'));
                if ($fidx!==false){
                    if ($newstock['stock'] < $oldstock[$fidx]['stock']){
                        if (!\Auth::user()->hasPermissionTo('minus stock')){
                            return redirect()->back()->withWarning("Edit gagal!")->with("text","Ada tidak bisa mengurangi stock produk");
                            break;
                        }
                    }
                }
            };
            $product->stock = json_encode($request->get('stock'));
            $product->description = $request->get('description');
            $product->is_active= $request->get('is_active');
            
            try {
                $path = \Storage::disk('public')->getAdapter()->getPathPrefix()."products/".$product->id."/";
                $images = $request->file('image');
                $imgnames = array();
                if (!empty($images)){
                    $imgs = json_decode($product->images);
                    foreach ($images as $key => $image) {
                        $imgname = slugify($product->title)."_img-".date("YmdHs")."-".$key.".jpg";
                        $imgnames[] = $imgname;
                        $img = \Image::make($image);
                        $img->fit(900)->save($path.$imgname,60);
                        $img->fit(300)->save($path."thumb/".$imgname,60);
                        $imgs[] = "storage/products/".$product->id."/".$imgname;
                    }
                    $product->images = json_encode($imgs);
                };
                if ($product->save()){
                    return redirect()->route('category',["id"=>$request->get('category')])->withSuccess("Edit berhasil!")->with("text","Perubahan pada produk berhasil disimpan");
                } else {
                    foreach ($imgnames as $key => $imag) {
                        \File::delete($path.$imag, $path."thumb/".$imag);
                    };
                    return redirect()->back()->withWarning("Edit gagal!")->with("text","Terjadi kesalahan, silahkan periksa kembali data yg anda masukkan");
                };
            } catch(\Exception $e){
                return redirect()->back()->withWarning("Penambahan gagal!")->with("text","Terjadi kesalahan saat menambahkan kategori baru"); 
            }
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }
     /**
     * edit product in database
     *
     * @param $request
     * @return \Illuminate\Http\Response
     
     */
    public function delete(Request $request){
        if (\Auth::user()->hasPermissionTo('delete produk-kategori')) {
            $product = Product::find($request->get('product_id'));
            $path = \Storage::disk('public')->getAdapter()->getPathPrefix()."products/".$product->id."/";
            foreach (json_decode($product->images,true) as $key => $image) {
                $imgdel = pathinfo($image,PATHINFO_BASENAME);
                \File::delete($path.$imgdel,$path."thumb/".$imgdel);
            }
            \File::deleteDirectory($path);
            if ($product->delete()){
                return redirect()->route('products')->withSuccess("produk dihapus!")->with("text","Produk telah berhasil dihapus dari database");
            } else {
                return redirect()->back()->withWarning("hapus gagal!")->with("text","Produk gagal dihapus dari sistem");
            }
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }
}
