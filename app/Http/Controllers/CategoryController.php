<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\DataTables\ProductsDataTable;
use App\DataTables\Scopes\ProductCategoryScope;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Show all the Admin user.
     *
     * @return \Illuminate\Http\Response
     */
    public function category(ProductsDataTable $datatable, Category $category) {
        if (\Auth::user()->hasPermissionTo('view produk-kategori')) {
            return $datatable->addScope(new ProductCategoryScope($category->id))->render('pages.products.category',['categories' => $category->children, 'current'=>$category]);
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * Show the add category page
     * 
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function viewAdd($id)
    {
        if (\Auth::user()->hasPermissionTo('edit produk-kategori')) {
            $tree = get_category_tree();
            $categoryTree = [];
            array_push($categoryTree, ['id' => 0, 'title' => 'TOP LEVEL','top' => 'top']);
            $traverse = function($categories,$depth=0) use(&$traverse,&$categoryTree){
                $cat=[];
                foreach ($categories as $category_single) {
                    $cat['id']=$category_single['id'];
                    $cat['title']='';
                    $cat['top']= $depth==0?'first':'';
                    for ($i=0; $i < $depth; $i++) { 
                        $cat['title'].= '&emsp;';
                    };
                    $cat['title'].=$category_single['title'];
                    array_push($categoryTree, $cat);
                    if (count($category_single['child'])>0) {
                        $traverse($category_single['child'],$depth+1);
                    };
                };
            };
            $traverse($tree);
            return view('pages.products.addcategory',['current'=>$id,'category_tree'=>$categoryTree]);
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * Show the edit category page
     * 
     * @param App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function viewEdit(Category $category)
    {
        if (\Auth::user()->hasPermissionTo('edit produk-kategori') && $category->title!="UNCATEGORIZED") {
            $tree = get_category_tree();
            $categoryTree = [];
            array_push($categoryTree, ['id' => 0, 'title' => 'TOP LEVEL','top' => 'top']);
            $traverse = function($categories,$depth=0) use(&$traverse,&$categoryTree,&$category){
                $cat=[];
                foreach ($categories as $category_single) {
                    $cat['id']=$category_single['id'];
                    $cat['title']='';
                    $cat['top']= $depth==0?'first':'';
                    for ($i=0; $i < $depth; $i++) { 
                        $cat['title'].= '&emsp;';
                    };
                    $cat['title'].=$category_single['title'];
                    if ($cat['id']!=$category->id && $category_single['title']!='UNCATEGORIZED') {
                        array_push($categoryTree, $cat);
                        if (count($category_single['child'])>0) {
                            $traverse($category_single['child'],$depth+1);
                        };
                    };
                };
            };
            $traverse($tree);
            
            return view('pages.products.editcategory',['current'=>$category,'category_tree'=>$categoryTree]);
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * add New category
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request){
        if (\Auth::user()->hasPermissionTo('edit produk-kategori')) {
            $newCat = Category::create([
                "title" => $request->get('title'),
                "parent_id" => $request->get('parent_id'),
                "created_by" => \Auth::user()->id
            ]);
            try {
                $path = \Storage::disk('public')->getAdapter()->getPathPrefix()."catpic/".$newCat->id."/";
                if ($request->hasFile('cover')){
                    $filename = slugify($newCat->title);
                    $filepath = "storage/catpic/".$newCat->id."/";
                    $img = \Image::make($request->file('cover'));
                    \File::makeDirectory($path);
                    $img->fit(900)->save($path.$filename.".jpg",60);
                    \File::makeDirectory($path."thumb/");
                    $img->fit(300)->save($path."thumb/".$filename.".jpg",60);
                    $newCat->cover = $filepath.$filename.".jpg";
                    $newCat->save();
                };
                if ($newCat->parent_id){
                    return redirect()->route('category',$newCat->parent)->withSuccess("Penambahan berhasil!")->with("text","Kategori baru telah berhasil ditambahkan");
                } else {
                    return redirect()->route('home')->withSuccess("Penambahan berhasil!")->with("text","Kategori baru telah berhasil ditambahkan");
                };
            } catch(\Exception $e){
                $newCat->delete();
                return redirect()->route('category',$newCat->parent)->withWarning("Penambahan gagal!")->with("text","Terjadi kesalahan saat menambahkan kategori baru"); 
            }
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }
    
    /**
     * Edit category
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request){
        $category = Category::find($request->get('current_id'));
        if (\Auth::user()->hasPermissionTo('edit produk-kategori') && $category->title!="UNCATEGORIZED") {
            $category->title = $request->get('title');
            $category->parent_id = $request->get('parent_id');
            try {
                if ($request->hasFile('cover')){
                    $path = \Storage::disk('public')->getAdapter()->getPathPrefix()."catpic/".$category->id."/";
                    $filename = slugify($category->title);
                    $oldName = pathinfo($category->cover,PATHINFO_BASENAME);
                    $filepath = "storage/catpic/".$category->id."/";
                    $img = \Image::make($request->file('cover'));
                    \File::delete($path.$oldName,$path."thumb/".$oldName);
                    $img->fit(900)->save($path.$filename.".jpg",60);
                    $img->fit(300)->save($path."thumb/".$filename.".jpg",60);
                    $category->cover = $filepath.$filename.".jpg";
                };
                if ($category->save()){
                    return redirect()->route('home')->withSuccess("Edit berhasil!")->with("text","Perubahan kategori telah berhasil disimpan");
                } else {
                    return redirect()->back()->withWarning("Edit gagal!")->with("text","Terjadi kesalahan, silahkan periksa kembali data yg dimasukkan.");
                };
            } catch(\Exception $e){
                return redirect()->route('category',$category->parent)->withWarning("Edit gagal!")->with("text","Terjadi kesalahan saat menambahkan kategori baru"); 
            }
        } else {
            return redirect()->back()->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * Delete a category
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request){
        $category = Category::find($request->get('cat_id'));
        if (\Auth::user()->hasPermissionTo('delete produk-kategori') && $category->title!="UNCATEGORIZED") {
            $parent = $category->parent_id;
            foreach ($category->children as $child){
                $child->parent_id = Category::where('title','UNCATEGORIZED')->select('id')->first()->id;
                $child->save();
            };
            \File::deleteDirectory(\Storage::disk('public')->getAdapter()->getPathPrefix()."catpic/".$category->id."/");
            if ($category->delete()){
                if ($parent){
                    return redirect()->route('category',['category'=>$parent])->withSuccess('Category Deleted!')->with('text','Sucessfully delete a category');
                }
                else {
                    return redirect()->route('home')->withSuccess('Category Deleted!')->with('text','Sucessfully delete a category');
                }
            }
            else{
                return redirect()->route('category',['category'=>$parent])->withWarning('Delete Fail!')->with('text', 'failed to delete category');
            };
        } else {
            return redirect()->back()->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
        //parse subcategory and data, move it to uncategorized        
    }
}
