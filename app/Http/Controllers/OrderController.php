<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\OrderDataTable;
use App\DataTables\ProductDataTable;
use App\Models\Order;
use App\Models\Product;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Show all products.
     *
     * @return \Illuminate\Http\Response
     */
    public function all(OrderDataTable $datatable) {
        if (\Auth::user()->hasPermissionTo('view order')) {
            return $datatable->render('pages.order.orders');
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        };
    } 

    /**
     * Show the add order page
     * 
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function add(ProductDataTable $datatable)
    {
        if (\Auth::user()->hasPermissionTo('add order')) {
            return $datatable->render('pages.order.addorder');
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        };
    }

    /**
     * Show the edit order page
     * 
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        if (\Auth::user()->hasPermissionTo('edit order')) {
            return view('pages.order.editorder',["order"=>$order]);
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        };
    }

    /**
     * store edited order.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        if (\Auth::user()->hasPermissionTo('edit order')) {
            $order = Order::create([
                "status" => 1,
                "payment" => 5,
                "shipping" => 0,
                "consignee" => $request->get('consignee'),
                "user_id" => \Auth::user()->id
            ]);
            try {
                foreach ($request->get('products') as $key => $product_id){
                    $product = Product::find($product_id);
                    $stocks = json_decode($product->stock,true);
                    $left=0;
                    $qty=0;
                    $details=$request->detail;
                    $orderDetail = array();
                    foreach ($details[$key] as $detail){
                        foreach ($stocks as $idx=>$stock){
                            if (($stock['variance']==$detail['variance'])&&($detail['count']>0)){
                                $stocks[$idx]['stock'] -= $detail['count'];
                                $left += ($stocks[$idx]['stock'] - $stocks[$idx]['keep']);
                                $orderDetail[] = $detail;
                            }
                        }
                        $qty += $detail['count'];
                    }
                    if (!$left){
                        $product->is_active = 0;
                    };
                    $product->stock = json_encode($stocks);
                    $product->save();
                    $level= get_offline_grid();
                    $price = $product->price_base + (($level->progress_type=='flat')?$level->price_progress:($level->price_progress*$product->price_base/100));
                    \DB::table('order_product')->insert([
                        'order_id' => $order->id,
                        'product_id' => $product->id,
                        'qty' => $qty,
                        'sell_price' => $price * $qty,
                        'detail'=> json_encode($orderDetail)
                    ]);
                };
                return redirect()->route('viewEditOrder',['order'=>$order])->withSuccess("Order ditambahkan!")->with("text","order berhasil ditambah secara manual");
            }catch (\Exception $e) {
                $order->delete();
                return redirect()->route('orders')->withWarning("Terjadi kesalahan")->with("text","Silahkan ulangi kembali beberapa saat lagi.");
            }
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        };
    }
    
    /**
     * store edited order.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        if (\Auth::user()->hasPermissionTo('edit order')) {
            $order = Order::find($request->get('current_id'));
            try {
                if ($request->get('status')==4 && $order->status!=4){
                    if ($order->status == 3){
                        return redirect()->route('orders')->withWarning("Gagal Menghapus")->with("text","Order yang telah selesai tidak bisa dibatalkan");
                    };
                    foreach ($order->products as $product){
                        $stocks = json_decode($product->stock,true);
                        $orderDetail = json_decode($product->pivot->detail,true);
                        $before = 0;
                        foreach ($orderDetail as $detail){
                            foreach ($stocks as $key => $item) {
                                if ($item['variance']==$detail['variance']){
                                    $before += ($item['stock']-$item['keep']);
                                    $stocks[$key]['stock'] += $detail['count'];
                                }
                            }
                        }
                        $product->stock = json_encode($stocks);
                        if (!$before){
                            $product->is_active = 1;
                        }
                        $product->save();
                    }
                };
                if ($order->status==4){
                    return redirect()->route('orders')->withWarning("Gagal Update")->with("text","Order yang telah dibatalkan tidak bisa diubah");
                };
                $order->status = $request->get('status');
                if ($request->has('shipping')){
                    $order->shipping = $request->get('shipping');
                };
                if ($request->has('tracking')){
                    $order->tracking = $request->get('tracking');
                }
                if ($request->has('delprod')){
                    foreach($request->delprod as $prod){
                        \DB::table('order_product')->where('order_id',$order->id)->where('product_id',$prod)->delete();
                    }
                }
                $order->save();
                return redirect()->route('orders')->withSuccess("Edit berhasil!")->with("text","order berhasil diubah");
            } catch (\Exception $e){
                return redirect()->back()->withWarning("Edit Gagal!")->with("text","Perubahan gagal, silahkan periksa kembali data yg dimasukkan");
            }
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        };
    }

    /**
     * delete order.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request) {
        if (\Auth::user()->hasPermissionTo('edit order')) {
            $order = Order::find($request->get('current_id'));
            $status = $order->getOrderStatus();
            if ($status=='selesai'){
                return redirect()->back()->withWarning("Gagal Menghapus")->with("text","Order yang telah selesai tidak bisa dihapus");
            };
            if ($status!='batal'){
                foreach ($order->products as $product){
                    $stocks = json_decode($product->stock,true);
                    $orderDetail = json_decode($product->pivot->detail,true);
                    $before = 0;
                    foreach ($orderDetail as $detail){
                        foreach ($stocks as $key => $item) {
                            if ($item['variance']==$detail['variance']){
                                $before += ($item['stock']-$item['keep']);
                                $stocks[$key]['stock'] += $detail['count'];
                            }
                        }
                    }
                    $product->stock = json_encode($stocks);
                    if (!$before){
                        $product->is_active = 1;
                    }
                    $product->save();
                }
            }
            $order->delete();
            return redirect()->route('orders')->withSuccess("Order dihapus!")->with("text","order berhasil dihapus dan stok dikembalikan");
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        };
    }

    /**
     * set Order Status.
     *
     * @return \Illuminate\Http\Response
     */
    public function setStatus(Request $request) {
        if (\Auth::user()->hasPermissionTo('edit order')) {
            $order = Order::find($request->get('current_id'));
            try {
                if ($request->get('status')==4 && $order->status!=4){
                    if ($order->status == 3){
                        return redirect()->route('orders')->withWarning("Gagal Menghapus")->with("text","Order yang telah selesai tidak bisa dibatalkan");
                    };
                    foreach ($order->products as $product){
                        $stocks = json_decode($product->stock,true);
                        $orderDetail = json_decode($product->pivot->detail,true);
                        $before = 0;
                        foreach ($orderDetail as $detail){
                            foreach ($stocks as $key => $item) {
                                if ($item['variance']==$detail['variance']){
                                    $before += ($item['stock']-$item['keep']);
                                    $stocks[$key]['stock'] += $detail['count'];
                                }
                            }
                        }
                        $product->stock = json_encode($stocks);
                        if (!$before){
                            $product->is_active = 1;
                        }
                        $product->save();
                    }
                };
                if ($order->status==4){
                    return redirect()->route('orders')->withWarning("Gagal Update")->with("text","Order yang telah dibatalkan tidak bisa diubah");
                };
                $order->status = $request->get('status');
                $order->save();
                return redirect()->back()->withSuccess("Status diubah!")->with("text","Status order berhasil diubah");
            } catch (\Exception $e){
                return redirect()->back()->withWarning("Edit Gagal!")->with("text","Perubahan gagal, silahkan coba beberapa saat lagi");
            }
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        };
    }

    /**
     * set Order Payment.
     *
     * @return \Illuminate\Http\Response
     */
    public function setPayment(Request $request) {
        if (\Auth::user()->hasPermissionTo('set payment')) {
            $order = Order::find($request->get('current_id'));
            try {
                $order->payment = $request->get('payment');
                $invoice = $order->invoice;
                if ($order->getPaymentStatus()=='lunas'||$order->payment==6){
                    $invoice->payment_date = \Carbon\Carbon::now('Asia/Jakarta');
                    $invoice->save();
                }
                $order->save();
                return redirect()->back()->withSuccess("Pembayaran diubah!")->with("text","Status pembayaran berhasil diubah");
            } catch (\Exception $e){
                return redirect()->back()->withWarning("Edit Gagal!")->with("text","Perubahan gagal, silahkan coba beberapa saat lagi");
            }
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        };
    }
}
