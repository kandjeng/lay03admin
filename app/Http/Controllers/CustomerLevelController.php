<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Level;
use App\DataTables\CustomerDataTable;
use App\DataTables\GridDataTable;
use Validator;

class CustomerLevelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Show all customers.
     *
     * @return \Illuminate\Http\Response
     */
    public function all(CustomerDataTable $datatable) {
        if (\Auth::user()->hasPermissionTo('view customer')) {
            return $datatable->render('pages.customer.customers');
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * Show edit customer page.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user) {
        if (\Auth::user()->hasPermissionTo('edit customer')) {
            $data = [
                'id' => $user->id,
                'name' => $user->name,
                'phone' => $user->phone,
                'email' => $user->email,
                'address' => $user->customer->address,
                'active' => $user->customer->active,
                'grid' => $user->customer->level
            ];
            return view('pages.customer.editcustomer',['cust'=>$data]);
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }
    
    /**
     * store edited customer page.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        if (\Auth::user()->hasPermissionTo('edit customer')) {
            $user = User::find($request->get('current_id'));
            $user->name= $request->get('name');
            $user->phone= $request->get('phone');
            $user->email= $request->get('email');
            $customer = $user->customer;
            $customer->address = $request->get('address');
            $customer->active= $request->get('active');
            $customer->level_id = $request->get('grid');
            
            if ($request->has('password')||$request->get('password')!=''){
                $user->password = \Hash::make($request->get('password'));
            };

            if ($user->save()&&$customer->save()){
                return redirect()->route('customers')->withSuccess("Edit berhasil!")->with("text","Data pelanggan berhasil diubah");
            } else {
                return redirect()->back()->withWarning("Edit Gagal!")->with("text","Penambahan gagal, silahkan periksa kembali data yg dimasukkan");
            }
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * toggle user/customer active state
     *
     * @return \Illuminate\Http\Response
     */
    public function toggleActive(Request $request) {
        if (\Auth::user()->hasPermissionTo('edit customer')) {
            $cust = User::find($request->get('current_id'))->customer;
            $cust->active = !$cust->active;
            $txt = ($cust->active)?'Aktivasi':'deAktivasi';
            if ($cust->save()){
                return redirect()->back()->withSuccess($txt." berhasil!")->with("text","Data pelanggan berhasil di-".$txt);
            } else {
                return redirect()->back()->withWarning($txt." Gagal!")->with("text",$txt." gagal, silahkan coba beberapa saat lagi");
            }
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * change user/customer grid level
     *
     * @return \Illuminate\Http\Response
     */
    public function setGrid(Request $request) {
        if (\Auth::user()->hasPermissionTo('edit grid')) {
            $cust = User::find($request->get('current_id'))->customer;
            $cust->level_id = $request->get('level_id');
            if ($cust->save()){
                return redirect()->back()->withSuccess("Grid berhasil diubah!")->with("text","Grid dari pelanggan berhasil diubah");
            } else {
                return redirect()->back()->withWarning("Edit grid Gagal!")->with("text","proses pengubahan grid gagal, silahkan coba beberapa saat lagi");
            }
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        };
    }
    
    /**
     * Show all available user grids
     *
     * @return \Illuminate\Http\Response
     */
    public function grids(GridDataTable $datatable) {
        if (\Auth::user()->hasPermissionTo('view grid')) {
            return $datatable->render('pages.customer.grids');
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }
    
    /**
     * Show edit customer grid page.
     *
     * @return \Illuminate\Http\Response
     */
    public function editGrid(Level $grid) {
        if (\Auth::user()->hasPermissionTo('edit grid')) {
            return view('pages.customer.editgrid',['grid'=>$grid]);
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * Show add customer grid page.
     *
     * @return \Illuminate\Http\Response
     */
    public function addGrid(Level $grid) {
        if (\Auth::user()->hasPermissionTo('edit grid')) {
            return view('pages.customer.addgrid');
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * Show customers from a grid.
     *
     * @return \Illuminate\Http\Response
     */
    public function gridCustomers(CustomerDataTable $datatable,Level $grid) {
        if (\Auth::user()->hasPermissionTo('view grid')) {
            $datatable->addConstraint($grid->id);
            return $datatable->render('pages.customer.gridcustomers');
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * add new grid customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function storeGrid(Request $request) {
        if (\Auth::user()->hasPermissionTo('edit grid')) {
            $rules = array(
                'title' => 'required|max:100',
                'progress_type' => 'required|max:11',
                'price_progress' => 'required'
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()){
                return redirect()->back()->withWarning('Validasi gagal')->with('text','Silahkan periksa kembali data yg anda masukkan. ', $validator->errors());
            };
            $grid = Level::create([
                'title'=>$request->get('title'),
                'progress_type'=>$request->get('progress_type'),
                'price_progress'=>$request->get('price_progress')
            ]);
            return redirect()->route('grids')->withSuccess("Grid Disimpan!")->with("text","Grid baru berhasil ditambahkan");
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * add new grid customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateGrid(Request $request) {
        if (\Auth::user()->hasPermissionTo('edit grid')) {
            $rules = array(
                'title' => 'required|max:100',
                'progress_type' => 'required|max:11',
                'price_progress' => 'required'
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()){
                return redirect()->back()->withWarning('Validasi gagal')->with('text','Silahkan periksa kembali data yg anda masukkan. ', $validator->errors());
            };
            try {
                $grid = Level::find($request->get('current_id'));
                $grid->title = $request->get('title');
                $grid->progress_type = $request->get('progress_type');
                $grid->price_progress = $request->get('price_progress');
                $grid->save();
                return redirect()->route('grids')->withSuccess("Grid Disimpan!")->with("text","Grid baru berhasil ditambahkan");
            } catch(\Exception $e){
                return redirect()->back()->withWarning('Edit gagal')->with('text','terjadi kesalahan,silahkan dicoba beberapa saat lagi');
            };
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * add new grid customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteGrid(Request $request) {
        if (\Auth::user()->hasPermissionTo('edit grid')) {
            try {
                $grid = Level::find($request->get('current_id'));
                $level = Level::whereNotIn('id',[$grid->id])->where('progress_type','percentage')->orderBy('price_progress','desc')->first();
                if (!isset($level)){
                    return redirect()->route('grids')->withWarning('Warning!')->with('text',"grid ini tidak boleh dihapus, periksa batasan penghapusan");
                } else {
                    foreach ($grid->customers as $customer) {
                        $customer->level()->associate($level);
                        $customer->save();
                    };
                    $grid->delete();
                    return redirect()->route('grids')->withSuccess("Grid Dihapus!")->with("text","Grid telah berhasil dihapus.");   
                }
            } catch(\Exception $e){
                return redirect()->back()->withWarning('Edit gagal')->with('text','terjadi kesalahan,silahkan dicoba beberapa saat lagi');
            };
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * show Offline customer grid.
     *
     * @return \Illuminate\Http\Response
     */
    public function offlineGrid() {
        if (\Auth::user()->hasPermissionTo('edit customer')) {
            return view('pages.customer.offlinegrid',['selected'=>\DB::table('settings')->where('setting','offline_grid')->first()->value]);
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }

    /**
     * update Offline customer grid.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateOffline(Request $request) {
        if (\Auth::user()->hasPermissionTo('edit customer')) {
            try {
                \DB::table('settings')->where('setting','offline_grid')->update(['value'=>$request->get('offline_grid')]);
                return redirect()->back()->withSuccess("Grid Offline Tersimpan!")->with("text","Grid customer offline berhasil diubah.");
            } catch (\Exception $e) {
                return redirect()->back()->withWarning('Update gagal')->with('text','terjadi kesalahan,silahkan dicoba beberapa saat lagi');
            }
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        }
    }
}
