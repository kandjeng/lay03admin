<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Order;
use App\Models\Product;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::where('status',3);
        $total = $orders->get()->sum(function($order){return $order->getTotal();});
        $topuser= User::with(['orders'=>function($query){$query->where('status',3);}])->where('is_admin',0)->get()->sortByDesc(function($user){return $user->orders->where("status",3)->sum(function($order){return $order->getTotal();});})->take(5);
        $lastOrder = Order::whereDate('created_at','>',Carbon::now()->subDays(7))->where('status',3)->get()->groupBy(function($date){return Carbon::parse($date->created_at)->format('Y-m-d');})->take(8);
        $prevOrder = array();
        for ($i=7; $i >= 0 ; $i--) { 
            $idx = Carbon::now()->subDays($i)->format('Y-m-d');
            $prevOrder[$idx]=(isset($lastOrder[$idx])?$lastOrder[$idx]:collect([]));
        }
        $data = [
            'user_count' => User::where('is_admin',0)->count(),
            'product_count' => Product::where('is_active',1)->count(),
            'order_count' => $orders->count(),
            'order_sum' => 'Rp '.money_short($total),
            'top_user' => $topuser,
            'prev_order' => $prevOrder,
        ];
        return view('home',$data);
    }
}
