<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\User;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Show report.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (\Auth::user()->hasPermissionTo('view order')) {
            return view('pages.order.report');
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        };
    }
    
    /**
     * Show report.
     *
     * @return \Illuminate\Http\Response
     */
    public function userReport(User $user){
        if (\Auth::user()->hasPermissionTo('view order')) {
            return view('pages.order.userreport',['user'=>$user]);
        } else {
            return redirect()->route('home')->withWarning("Akses Ditolak")->with("text","Anda tidak diperbolehkan mengakses halaman ini.");
        };
    }

    /**
     * get the datatable for order in general
     *
     * @return \Illuminate\Http\Response
     */
    public function getOrdersDone(Request $request)
    {
        $start = $request->get('start');
        $end = $request->get('end');
        $type= $request->get('type');
        $orders = Order::select(['id', 'status', 'payment','shipping', 'consignee', 'created_at','user_id'])
                    ->whereDate('created_at','>',$start)->whereDate('created_at','<',$end);
        if ($type){
            $orders = $orders->where('payment',$type);
        }
        $statuses = \DB::table('order_statuses')->pluck('status','id');
        return \DataTables::of($orders)
        ->editColumn('id','#{{sprintf("%06d",$id)}}')
        ->editColumn('created_at',function($query){
            return $query->created_at->format('Y/m/d H:i');
        })
        ->editColumn('basetotal',function($query){
            return 'Rp. '.number_format($query->baseTotal(),0,',','.');
        })
        ->editColumn('shipping',function($query){
            return 'Rp '.number_format($query->shipping,0,',','.');
        })
        ->editColumn('subtotal',function($query){
            return 'Rp '.number_format($query->getTotal(),0,',','.');
        })
        ->editColumn('payment',function ($query)use($statuses){
            return $statuses[$query->payment];
        })
        ->editColumn('total',function($query){
            return 'Rp '.number_format(($query->shipping + $query->getTotal()),0,',','.');
        })
        ->make();
    }
    /**
     * get the datatable for order of a user
     *
     * @return \Illuminate\Http\Response
     */
    public function getOrdersUser(Request $request)
    {
        $start = $request->get('start');
        $end = $request->get('end');
        $type= $request->get('type');
        $user= User::find($request->get('user_id'));
        $orders = Order::select(['id', 'status', 'payment','shipping', 'consignee', 'created_at','user_id'])
                    ->whereDate('created_at','>',$start)->whereDate('created_at','<',$end)->where('user_id',$user->id);
        if ($type){
            $orders = $orders->where('payment',$type);
        }
        $statuses = \DB::table('order_statuses')->pluck('status','id');
        return \DataTables::of($orders)
        ->editColumn('id','#{{sprintf("%06d",$id)}}')
        ->editColumn('created_at',function($query){
            return $query->created_at->format('Y/m/d H:i');
        })
        ->editColumn('basetotal',function($query){
            return 'Rp. '.number_format($query->baseTotal(),0,',','.');
        })
        ->editColumn('shipping',function($query){
            return 'Rp '.number_format($query->shipping,0,',','.');
        })
        ->editColumn('subtotal',function($query){
            return 'Rp '.number_format($query->getTotal(),0,',','.');
        })
        ->editColumn('payment',function ($query)use($statuses){
            return $statuses[$query->payment];
        })
        ->editColumn('total',function($query){
            return 'Rp '.number_format(($query->shipping + $query->getTotal()),0,',','.');
        })
        ->make();
    }
}
