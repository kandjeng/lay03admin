<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Customer;
use App\Models\Level;
use Illuminate\Http\Request;
use App\Http\Requests;
use JWTAuth;
use Response;
use App\Repository\Transformers\CustomerTransformer;
use \Illuminate\Http\Response as Res;
use Validator;
use Tymon\JWTAuth\Exceptions\JWTException;

class CustomerController extends ApiController
{
    /**
     * @var \App\Repository\Transformers\CustomerTransformer
     * */
    protected $customerTransformer;

    public function __construct(CustomerTransformer $customerTransformer)
    {
        $this->customerTransformer = $customerTransformer;
    }

    /**
     * @description: Api user authenticate method
     * 
     * @param: email, password
     * @return: Json String response
     */
    public function authenticate(Request $request)
    {
        $rules = array (
            'phone' => 'required|',
            'password' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator-> fails()){
            return $this->respondValidationError('Validasi login gagal.', $validator->errors());
        }
        else{
            $user = User::where('phone', $request['phone'])->first();
            if($user){
                $access_token = $user->access_token;
                if ($access_token == NULL){
                    return $this->_login($request['phone'], $request['password']);
                }
                try{
                    $user = JWTAuth::setToken($access_token)->toUser();
                    return $this->respond([
                        'status' => 'success',
                        'status_code' => $this->getStatusCode(),
                        'message' => 'sudah login',
                        'user' => $this->customerTransformer->transform($user)
                    ]);
                }catch(JWTException $e){
                    $user->access_token = NULL;
                    $user->save();
                    return $this->respondInternalError("Login gagal, terjadi kesalahan saat melakukan login!");
                }
            }
            else{
                return $this->respondWithError("Phone atau Password invalid");
            }
        }
    }
    
    private function _login($phone, $password)
    {
        $credentials = ['phone' => $phone, 'password' => $password];
        
        if ( ! $token = JWTAuth::attempt($credentials)) {
            if (User::where('phone', $phone)->first()){
                return $this->respondWithError("Password salah!");    
            } else {
                return $this->respondWithError("User belum terdaftar!");
            }
        }
        $user = JWTAuth::setToken($token)->toUser();
        $user->access_token = $token;
        $user->save();
        return $this->respond([
            'status' => 'success',
            'status_code' => $this->getStatusCode(),
            'message' => 'Login berhasil!',
            'user' => $this->customerTransformer->transform($user)
        ]);
    }

    /**
     * @description: Api user register method
     * 
     * @param: lastname, firstname, username, email, password
     * @return: Json String response
     */
    public function register(Request $request)
    {
        $rules = array (
            'name' => 'required|max:255',
            'phone' => 'required|max:15|unique:users,phone',
            'address' => 'required|min:5',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:3'
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator-> fails()){
            return $this->respondValidationError('Validasi data gagal. ', $validator->errors());
        }
        else{
            $user = User::create([
                'name' => $request['name'],
                'phone' => $request['phone'],
                'password' => \Hash::make($request['password']),
            ]);
            $level = Level::where('progress_type','flat')->orderBy('price_progress','desc')->first();
            if ($level==null)
                $level = Level::where('progress_type','percentage')->orderBy('price_progress','desc')->first();
            $customer = Customer::create([
                'address' => $request['address'],
                'user_id' => $user->id,
                'level_id' =>$level->id,
                'active' => 1
            ]);
            return $this->_login($request['phone'], $request['password']);
        }
    }

    /**
     * @description: update profile customer in app
     * 
     * @param: name, email, address
     * @return: Json String response
     */
    public function updateProfile(Request $request)
    {
        try{
            $rules = array (
                'name' => 'required|max:255',
                'address' => 'required|min:5'
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator-> fails()){
                return $this->respondValidationError('Validasi data gagal. ', $validator->errors());
            }
            else{
                if ($user = JWTAuth::setToken($request->header('X-Auth-Token'))->toUser()){
                    $user->name = $request['name'];
                    $user->email = $request['email'];
                    $customer = $user->customer;
                    $customer->address = $request['address'];
                    $user->save();
                    $customer->save();
                    return $this->respond([
                        'status' => 'success',
                        'status_code' => $this->getStatusCode(),
                        'message' => 'Profile berhasil ter update.',
                        'profile' => $request->all(),
                    ]);
                } else {
                    return $this->respond([
                        'status' => 'success',
                        'status_code' => 266,
                        'message' => 'Profil tidak ditemukan, silahkan login kembali',
                        'profile' => $request->all(),
                    ]);
                }
            }
        }catch(JWTException $e){
            return $this->respondInternalError("Terjadi kesalahan saat melakukan update!");
        } 
    }

    /**
     * @description: Change user password in App
     * 
     * @param: lastname, firstname, username, email, password
     * @return: Json String response
     */
    public function changePass(Request $request)
    {
        $token = $request->header('X-Auth-Token');
        try{
            $rules = array (
                'old_pass' => 'required|min:6',
                'new_pass' => 'required|min:6',
                'new_pass_confirm' => 'required|min:6',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator-> fails()){
                return $this->respondValidationError('Validasi data gagal. ', $validator->errors());
            }
            else{
                if ($user = JWTAuth::setToken($token)->toUser()){
                    if (\Hash::check($request->get('old_pass'), $user->password)){
                        $user->password = \Hash::make($request['new_pass']);
                        $user->save();
                        return $this->respond([
                            'status' => 'success',
                            'status_code' => $this->getStatusCode(),
                            'message' => 'Password berhasil diperbarui.',
                        ]);
                    } else {
                        return $this->respond([
                            'status' => 'error',
                            'status_code' => 212,
                            'message' => 'password lama tidak cocok.'
                        ]);    
                    }
                } else {
                    return $this->respond([
                        'status' => 'error',
                        'status_code' => 266,
                        'message' => 'Profil tidak ditemukan, silahkan login kembali'
                    ]);
                }
            }
        }catch(JWTException $e){
            return $this->respondInternalError("Terjadi kesalahan saat melakukan update!");
        } 
    }

    /**
     * @description: Api user logout method
     * 
     * @param: null
     * @return: Json String response
     */
    public function logout($access_token)
    {
        try{
            $user = JWTAuth::setToken($access_token)->toUser();
            $user->access_token = NULL;
            $user->save();
            JWTAuth::setToken($access_token)->invalidate();
            $this->setStatusCode(Res::HTTP_OK);
            return $this->respond([
                'status' => 'success',
                'status_code' => $this->getStatusCode(),
                'message' => 'Berhasil logout!',
            ]);

        }catch(JWTException $e){
            return $this->respondInternalError($e);
        }
    }

    /**
     * @description: Check user active
     * 
     * @param: null
     * @return: Json String response
     */
    public function active($token)
    {
        try{
            if ($user = JWTAuth::setToken($token)->toUser()){
                return $this->respond([
                    'status' => 'success',
                    'status_code' => $this->getStatusCode(),
                    'message' => 'Status aktivasi diambil.',
                    'active' => $user->customer->active
                ]);
            } else {
                $user->access_token=null;
                $user->save();
                return $this->respond([
                    'status' => 'error',
                    'status_code' => 266,
                    'message' => 'Session habis, silahkan login kembali.',
                    'active' => 0
                ]);
            }

        }catch(JWTException $e){
            return $this->respondInternalError("Terjadi kesalahan saat mengambil status aktif!");
        }
    }
}