<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['status','payment','shipping','user_id','consignee'];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function products(){
        return $this->belongsToMany('App\Models\Product')->withPivot('qty','detail','sell_price');
    }
    public function invoice(){
        return $this->hasOne('App\Models\Invoice');
    }
    public function getPaymentStatus(){
        return \DB::table('order_statuses')->where('id',$this->payment)->first()->status;
    }
    public function getOrderStatus(){
        return \DB::table('order_statuses')->where('id',$this->status)->first()->status;
    }
    public function baseTotal(){
        $total=0;
        $this->products->each(function($item,$key)use(&$total){
            $total += ($item->pivot->qty * $item->price_base);
        });
        return $total;
    }
    public function getTotal(){
        $total =0;
        $this->products->each(function($item,$key) use (&$total){
            // $total += $item->pivot->sell_price * $item->pivot->qty;
            $total += $item->pivot->sell_price ;
        });
        return $total;
    }
    public function getTotalWeight(){
        $total =0;
        $this->products->each(function($item,$key) use (&$total){
            $total += $item->weight * $item->pivot->qty;
        });
        return $total;
    }
    public function getCount(){
    	return $this->products->sum(function($prod){return $prod->pivot->qty;});
    }
}
