<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Level;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['sku','title','price_base','description','stock','weight','images','created_by','category_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['created_by'];

    /**
     * get the user account created the product
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function uploader()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * get the category of this Product
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function orders(){
        return $this->belongsToMany('App\Models\Order')->withPivot('qty','detail');
    }

    public function markupPrice(Level $level){
        return (($level->progress_type=='flat')?$level->price_progress:($this->price_base * $level->price_progress /100)) + $this->price_base;
    }
    public function getStock(){
    	return collect(json_decode($this->stock,true))->sum(function ($prod){return $prod['stock'] - $prod['keep'];});
    }
}
