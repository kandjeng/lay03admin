<?php

namespace App\DataTables;

use App\Models\Order;
use Yajra\DataTables\Services\DataTable;

class OrderDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('user_id',function($query){
                return $query->consignee.(($query->user->is_admin)?' (Offline)':'');
            })
            ->editColumn('status',function($query){
                if (\Auth::user()->hasPermissionTo('edit order')&&$query->status<3){
                    $txt = '<select data-order="'.$query->id.'" class="select-change status-changer" data-default="'.$query->status.'" data-name="'.$query->user->name.'">';
                    foreach (\DB::table('order_statuses')->where('identity','status')->get() as $status) {
                        $txt .= '<option value="'.$status->id.'" '.(($query->status==$status->id)?'selected':'').'>'.$status->status.'</option>';
                    };
                    return $txt."</select>";
                } else {
                    return '<span class="emphas">'.\DB::table('order_statuses')->where('id',$query->status)->first()->status.'</span>';
                };
            })
            ->editColumn('created_at',function($query){
                return $query->created_at->format('d M Y');
            })
            ->editColumn('payment',function($query){
                return '<span class="emphas">'.\DB::table('order_statuses')->where('id',$query->payment)->first()->status.'</span>';
                
            })
            ->addColumn('subtotal',function($query){
                return number_format($query->getTotal(),0,',','.');
            })
            ->editColumn('shipping',function($query){
                return number_format($query->shipping,0,',','.');
            })
            ->addColumn('total',function($query){
                return 'Rp. '.number_format($query->getTotal()+$query->shipping,0,',','.');
            })
            ->addColumn('action', function($query){
                if (\Auth::user()->hasPermissionTo('edit order')){
                    return '<a href="'.route('viewEditOrder',['id'=>$query->id]).'" class="btn btn-xs btn-info" ><i class="fa fa-edit"></i> edit order</a>';
                } else {
                    return '';
                }
            })
            ->addColumn('invoice',function($query){
                if ($query->status!=4){
                    if ($query->invoice){
                        return '<a href="'.route('viewEditInvoice',['invoice'=> $query->invoice ]).'" class="btn btn-warning">Lihat Invoice</a>';
                    } else {
                        return '<button data-order="'.$query->id.'" class="btn btn-default btn-invoice">Buat Invoice</button>';
                    }
                }
            })
            ->setRowClass(function ($query) {
                $cls = [1=>'row-info',2=>"row-default",3=>'row-success',4=>'row-warning'];
                return $cls[$query->status];
            })
            ->rawColumns(['action','status','payment','invoice']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {
        return $model->newQuery()->select('id', 'status', 'consignee', 'payment','shipping','user_id', 'created_at');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters([
                        'dom' => 'flrti',
                        'order'   => [[0, 'desc']],
                        "lengthMenu"=> [[-1],['all']]
                    ]);
    }
    
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Tgl Order'],
            'status',
            'payment',
            ['data' => 'user_id', 'name' => 'user_id', 'title' => 'Customer'],
            'subtotal',
            ['data' => 'shipping', 'name' => 'shipping', 'title' => 'Ongkir'],
            'total',
            'invoice'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Order_' . date('YmdHis');
    }
}
