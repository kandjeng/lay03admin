<?php

namespace App\DataTables;

use App\Models\Level;
use Yajra\DataTables\Services\DataTable;

class GridDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('progress_type',function($query){
                return (($query->progress_type=='percentage')?'Persentase':'Flat').': '.$query->price_progress.(($query->progress_type=='percentage')?' (%)':' (Rp.)');
            })
            ->addColumn('customer',function($query){
                return '<a href="'.route('viewGridCustomer',['id'=>$query->id]).'" class="btn btn-default">Daftar Pelanggan</a>';
            })
            ->addColumn('action', function($query){
                if (\Auth::user()->hasPermissionTo('edit grid')){
                    return '<a href="'.route('viewEditGrid',['id'=>$query->id]).'" class="btn btn-xs btn-info" ><i class="fa fa-edit"></i> edit/hapus grid</a>';
                } else {
                    return '';
                }
            })
            ->rawColumns(['action','customer']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Level $model)
    {
        return $model->newQuery()->select('id', 'title', 'progress_type', 'price_progress');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters(['dom' => 'lifrt',
                    "lengthMenu"=> [[-1],['all']],]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'title', 'name' => 'title', 'title' => 'Nama'],
            ['data' => 'progress_type', 'name' => 'progress_type', 'title' => 'Progresi Harga'],
            'customer'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Grid_' . date('YmdHis');
    }
}
