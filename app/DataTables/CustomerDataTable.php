<?php

namespace App\DataTables;

use App\Models\User;
use App\Models\Level;
use Yajra\DataTables\Services\DataTable;

class CustomerDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function($query){
                if (\Auth::user()->hasPermissionTo('edit customer')){
                    return '<a href="'.route('viewEditCustomer',['id'=>$query->user_id]).'" class="btn btn-info" ><i class="fa fa-edit"></i> edit</a>';
                } else {
                    return '';
                };
            })
            ->addColumn('grid', function($query){
                if (\Auth::user()->hasPermissionTo('edit grid')){
                    $txt = '<select data-customer="'.$query->user_id.'" class="select-change grid-changer" data-default="'.$query->level_id.'" data-name="'.$query->name.'">';
                    foreach (Level::all() as $level) {
                        $txt .= '<option value="'.$level->id.'" '.(($query->level_id==$level->id)?'selected':'').'>'.$level->title.': '.$level->price_progress.(($level->progress_type=='flat')?' (Rp.)':' (%)').'</option>';
                    };
                    return $txt."</select>";
                } else {
                    return $query->title.": ".$query->price_progress.(($query->progress_type=="flat")?" (Rp)":" (%)");
                }
            })
            ->addColumn('aktivasi',function($query){
                return '<button type="button" class="btn btn-activate '.($query->active?'btn-danger':'btn-success').'" data-name="'.$query->name.'" data-customer="'.$query->user_id.'">'.($query->active?'nonaktifkan':'aktifkan').'</button>';
            })
            ->editColumn('active', function($query){
                return ($query->active)?'aktif':'nonaktif';
            })
            ->editColumn('history', function($query){
                return "<a href='".route('reportUser',['id'=>$query->user_id])."' class='btn btn-warning'><i class='fa fa-tasks'></i> History Order</a>";
            })
            ->setRowClass(function ($query) {
                return ($query->active)?'row-success' : 'row-warning';
            })
            ->rawColumns(['action','grid','aktivasi','history']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {   
        if (isset($this->id)){
            return $model->newQuery()
                ->join('customers','users.id','=','customers.user_id')
                ->join('levels','levels.id','=','customers.level_id')
                ->select('customers.user_id','users.name','users.phone','customers.address','customers.active','customers.id','customers.level_id','levels.title','levels.progress_type','levels.price_progress')
                ->where('customers.level_id',$this->id)
                ->get();
        }else{
            return $model->newQuery()
            ->join('customers','users.id','=','customers.user_id')
            ->join('levels','levels.id','=','customers.level_id')
            ->select('customers.user_id','users.name','users.phone','customers.address','customers.active','customers.id','customers.level_id','levels.title','levels.progress_type','levels.price_progress')
            ->get();
        }
    }

    public function addConstraint($id){
        $this->id = $id;
    }
    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters([
                        'dom' => 'flrti',
                        'order'   => [[4, 'asc'],[0,'desc']],
                        "lengthMenu"=> [[-1],['all']]
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'phone',
            'name',
            'grid',
            'active',
            'aktivasi',
            'history'

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Customer_' . date('YmdHis');
    }
}
