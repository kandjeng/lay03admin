<?php

namespace App\DataTables;

use App\Models\Invoice;
use App\Models\Order;
use Yajra\DataTables\Services\DataTable;

class InvoiceDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('payment_date',function($query){
                return ($query->payment_date?$query->payment_date:'BELUM');
            })
            ->addColumn('consignee',function($query){
                return $query->consignee.(($query->order->user->is_admin)?' (Offline)':'');
            })
            ->editColumn('created_at',function($query){
                return $query->created_at->format('d/m/Y');
            })
            ->addColumn('payment',function($query){
                if (\Auth::user()->hasPermissionTo('set payment')){
                    $txt = '<select data-default="'.$query->payment.'" data-order="'.$query->order_id.'" class="select-change payment-changer" data-name="'.$query->consignee.'">';
                    foreach (\DB::table('order_statuses')->where('identity','payment')->get() as $status) {
                        $txt .= '<option value="'.$status->id.'" '.(($query->payment==$status->id)?'selected':'').'>'.$status->status.'</option>';
                    };
                    return $txt."</select>";
                } else {
                    return '<span class="emphas">'.\DB::table('order_statuses')->where('id',$query->payment)->first()->status.'</span>';
                };
            })
            ->addColumn('action', function($query){
                return "<a href='".route('viewEditInvoice',['id'=>$query->id])."' class='btn btn-xs btn-info'><i class='fa fa-eye'></i> lihat</a>";
            })
            ->addColumn('total',function($query){
                return 'Rp '.number_format(Order::find($query->order_id)->getTotal(),0,',','.');
            })
            ->setRowClass(function ($query) {
                $cls = [5=>'row-info',6=>'row-success',7=>'row-warning'];
                return $cls[$query->order->payment];
            })
            ->rawColumns(['action','payment']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Invoice $model)
    {
        return $model->newQuery()->join('orders','orders.id','=','invoices.order_id')
            ->select('invoices.id', 'invoices.order_id', 'invoices.invoice_number','invoices.payment_date', 
                'invoices.created_at','orders.consignee','orders.payment')->get();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters(['dom' => 'lifrt',
                        'order'   => [[2, 'desc']],
                        "lengthMenu"=> [[-1],['all']],]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Tgl Dibuat'],
            'consignee',
            ['data' => 'invoice_number', 'name' => 'invoice_number', 'title' => 'no. invoice'],
            ['data' => 'payment_date', 'name' => 'payment_date', 'title' => 'Tgl Dibayar'],
            'payment',
            'total',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Invoice_' . date('YmdHis');
    }
}
