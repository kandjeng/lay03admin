<?php

namespace App\DataTables;

use App\Models\Product;
use Yajra\DataTables\Services\DataTable;

class ProductsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('stock', function($query){
                $txt = '';
                foreach(json_decode($query->stock,true) as $idx => $stok){
                    
                    $txt .= ($idx==0?'':'<br>').$stok['variance'].': '.$stok['stock'].(($stok['keep']==0)?'':'(keep:'.$stok['keep'].')');
                };
                return $txt;
            })
            ->editColumn('is_active',function($query){
                return ($query->is_active)?'<strong>Aktif</strong>':'<strong>nonaktif</strong>';
            })
            ->editColumn('images', function($query){
                if ($query->images!='[]'){
                    $img = json_decode($query->images,true)[0];
                    return '<img src="'.asset(pathinfo($img,PATHINFO_DIRNAME).'/thumb/'.pathinfo($img,PATHINFO_BASENAME)).'" alt="gbr-produk" class="img-table-list">' ;
                }else{
                    return '<img src="'.asset('images/none.jpg').'" alt="gbr-produk" class="img-table-list">' ;
                }
            })
            ->addColumn('action', function($query){
                if (\Auth::user()->hasPermissionTo('edit produk-kategori')) {
                    return '<a href="'.route('viewEditProduct',['id'=>$query->id]).'" class="btn btn-xs btn-info" ><i class="fa fa-edit"></i> edit/hapus</a>';
                } else {
                    return '';
                };
            })
            ->setRowClass(function ($query) {
                return ($query->is_active)?'' : 'row-warning';
            })
            ->rawColumns(['action','stock','images','is_active']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Product $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Product $model)
    {
        return $model->newQuery()->select('id', 'sku', 'is_active','title', 'price_base', 'stock','images');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters(['dom' => 'lifrt',
                    	"lengthMenu"=> [[-1],['all']],
                    	"scrollY" => 450,
		        "deferRender" => true,
			"scroller" => true
                    	]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'images', 'name' => 'images', 'title' => 'Gambar'],
            ['data' => 'title', 'name' => 'title', 'title' => 'Nama Produk'],
            ['data' => 'sku', 'name' => 'sku', 'title' => 'KodeSKU'],
            ['data' => 'price_base', 'name' => 'price_base', 'title' => 'Harga Dasar'],
            ['data' => 'stock', 'name' => 'stock', 'title' => 'Stok'],
            ['data' => 'is_active', 'name' => 'is_active', 'title' => 'Aktif']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Product_' . date('YmdHis');
    }
}
