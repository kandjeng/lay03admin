<?php

namespace App\DataTables;

use \App\Models\User;
use Yajra\DataTables\Services\DataTable;

class AdminDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        if (\Auth::user()->hasPermissionTo('edit admin')) {
            return datatables($query)
                ->addColumn('peran', function($query){
                    return $query->getRoleNames()->implode(', ');
                })
                ->addColumn('action', function($query){
                    return '<a href="'.route('editProfile',['id'=>$query->id]).'" class="btn btn-xs btn-info" ><i class="fa fa-edit"></i> edit/hapus</a>';
                })
                ->rawColumns(['action']);
        }
        else {
            return datatables($query)
                ->addColumn('peran', function($query){
                    return $query->getRoleNames()->implode(', ');
                });
        }
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery()->select('id', 'phone','name','email')->where('is_admin',1)->orderBy('id','asc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters(['dom' => 'lifrt',
                    "lengthMenu"=> [[-1],['all']],]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'name', 'name' => 'name', 'title' => 'Nama Admin'],
            'email',
            'phone',
            'peran'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Admin_' . date('YmdHis');
    }
}
