<?php

namespace App\DataTables;

use App\Models\Product;
use Yajra\DataTables\Services\DataTable;

class ProductDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        if (\Auth::user()->hasPermissionTo('edit produk-kategori')) {
            return datatables($query)
                ->editColumn('images', function($query){
                    if ($query->images!='[]'){
                        $img = json_decode($query->images,true)[0];
                        return '<img src="'.asset(pathinfo($img,PATHINFO_DIRNAME).'/thumb/'.pathinfo($img,PATHINFO_BASENAME)).'" alt="gbr-produk" class="img-table-list">' ;
                    }else{
                        return '<img src="'.asset('images/none.jpg').'" alt="gbr-produk" class="img-table-list">' ;
                    }
                })
                ->editColumn('price_base',function($query){
                    $level = get_offline_grid();
                    return $query->price_base + (($level->progress_type=='flat')?$level->price_progress:($level->price_progress*$query->price_base/100));
                })
                ->editColumn('stock', function($query){
                    $txt = '';
                    foreach(json_decode($query->stock,true) as $idx => $stok){
                        
                        $txt .= ($idx==0?'':'<br>').$stok['variance'].': '.($stok['stock']-$stok['keep']);
                    };
                    return $txt;
                })
                ->addColumn('action', function($query){
                    $level = get_offline_grid();
                    $price = $query->price_base + (($level->progress_type=='flat')?$level->price_progress:($level->price_progress*$query->price_base/100));
                    return "<button class='btn btn-primary btn-addproduct' data-price='$price' data-id='$query->id' data-name='$query->sku' data-stock='$query->stock'><i class='fa fa-plus'></i> Tambah</button>";
                })
                ->rawColumns(['action','images','stock']);
        }
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Product $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Product $model)
    {
        return $model->newQuery()->select('id', 'sku', 'is_active','title', 'price_base', 'images', 'stock')->where('is_active',1);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'images', 'name' => 'images', 'title' => 'Gambar'],
            'sku',
            ['data' => 'title', 'name' => 'title', 'title' => 'Nama Produk'],
            ['data' => 'price_base', 'name' => 'price_base', 'title' => 'Harga'],
            ['data' => 'stock', 'name' => 'stock', 'title' => 'Stok']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Product_' . date('YmdHis');
    }
}
