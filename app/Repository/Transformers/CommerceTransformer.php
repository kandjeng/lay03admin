<?php
namespace App\Repository\Transformers;
use App\Models\Order;
use App\Models\Level;

class CommerceTransformer extends Transformer{
    public function transformCat($category){
        return [
            'idKategori' => $category->id,
            'namaKategori' => $category->title,
            'slugKategori' => slugify($category->title,50),
            'gambarKategori' => $category->cover,
            'thumbKategori' => pathinfo($category->cover,PATHINFO_DIRNAME)."/thumb/".pathinfo($category->cover,PATHINFO_BASENAME),
        ];
    }
    public function transform($product){
        $img = json_decode($product->images)[0];
        return [
            'idProduk' => $product->id,
            'sku' => $product->sku,
            'namaProduk' => $product->title,
            'harga' =>$product->price_base, //change to modified price
            'images' => json_decode($product->images),
            'thumbs' => collect(json_decode($product->images,true))->map(function($item, $key){return pathinfo($item,PATHINFO_DIRNAME)."/thumb/".pathinfo($item,PATHINFO_BASENAME);}),
            'thumb' => pathinfo($img,PATHINFO_DIRNAME)."/thumb/".pathinfo($img,PATHINFO_BASENAME),
        ];
    }
    public function transformMarkup($product,$level){
        if ($product->images!='[]'){
            $img = json_decode($product->images)[0];
            $thumb = pathinfo($img,PATHINFO_DIRNAME)."/thumb/".pathinfo($img,PATHINFO_BASENAME);
        }else{
            $thumb = 'images/none.jpg';
        }
        $markup = ($level->progress_type=='flat')? $level->price_progress: ($level->price_progress*$product->price_base/100);
        return [
            'idProduk' => $product->id,
            'sku' => $product->sku,
            'namaProduk' => $product->title,
            'price' =>$markup + $product->price_base,
            'thumb' => $thumb,
            'stock' => collect(json_decode($product->stock))->map(function($item,$value){
                return [
                    "variance" => $item->variance,
                    "stock" => $item->stock - $item->keep
                ];
            })
        ];
    }
    public function transformProd($product,$level){
        $img = json_decode($product->images)[0];
        $markup = ($level->progress_type=='flat')? $level->price_progress: ($level->price_progress*$product->price_base/100);
        return [
            'id' => $product->id,
            'sku' => $product->sku,
            'name' => $product->title,
            'price' => $product->price_base + $markup,
            'weight' => $product->weight,
            'short_description' => $product->description,
            'images' => json_decode($product->images),
            'thumbs' => collect(json_decode($product->images,true))->map(function($item, $key){return pathinfo($item,PATHINFO_DIRNAME)."/thumb/".pathinfo($item,PATHINFO_BASENAME);}),
            'thumb' => pathinfo($img,PATHINFO_DIRNAME)."/thumb/".pathinfo($img,PATHINFO_BASENAME),
            'stock' => collect(json_decode($product->stock))->map(function($item,$value){
                return [
                    "variance" => $item->variance,
                    "stock" => $item->stock - $item->keep
                ];
            })
        ];
    }
    public function transformOrder(Order $order){
        return [
            "id" => sprintf("%06d", $order->id),
            "status" => $order->getOrderStatus(),
            "date" =>$order->created_at->format('d/m/Y'),
            "payment" => $order->getPaymentStatus(),
            "shipping" => $order->shipping,
            "tracking" => $order->tracking,
            "totalWeight" => $order->getTotalWeight(),
            'totalCount' => $order->getCount(),
            "total" => $order->getTotal() + $order->shipping,
            "products" => $order->products->map(function($item,$key){
                $det = '';
                foreach (json_decode($item->pivot->detail,true) as $d){
                    $det .= $d['variance'].': '.$d['count'].', ';
                };
                return [
                    "sku" => $item->sku,
                    "name" => $item->title,
                    "price" => $item->pivot->sell_price,
                    "count" => $item->pivot->qty,
                    "details" => $det
                ];
            })
        ];
    }
    public function transformProducts($products,$level){
        return collect($products)->map(function ($item,$key)use($level){
            return $this->transformMarkup($item, $level);
        });
    }
    
}