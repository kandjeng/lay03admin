<?php
namespace App\Repository\Transformers;

class CustomerTransformer extends Transformer{
    public function transform($user){
        return [
            'fullname' => $user->name,
            'phone' => $user->phone,
            'email' => $user->email,
            'address' => $user->customer->address,
            'active' => $user->customer->active,
            'token' => $user->access_token,
        ];
    }
}