<?php

return [

    'models' => [

        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * Eloquent model should be used to retrieve your permissions. Of course, it
         * is often just the "Permission" model but you may use whatever you like.
         *
         * The model you want to use as a Permission model needs to implement the
         * `Spatie\Permission\Contracts\Permission` contract.
         */

        'permission' => Spatie\Permission\Models\Permission::class,

        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * Eloquent model should be used to retrieve your roles. Of course, it
         * is often just the "Role" model but you may use whatever you like.
         *
         * The model you want to use as a Role model needs to implement the
         * `Spatie\Permission\Contracts\Role` contract.
         */

        'role' => Spatie\Permission\Models\Role::class,

    ],

    'table_names' => [

        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * table should be used to retrieve your roles. We have chosen a basic
         * default value but you may easily change it to any table you like.
         */

        'roles' => 'roles',

        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * table should be used to retrieve your permissions. We have chosen a basic
         * default value but you may easily change it to any table you like.
         */

        'permissions' => 'permissions',

        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * table should be used to retrieve your models permissions. We have chosen a
         * basic default value but you may easily change it to any table you like.
         */

        'model_has_permissions' => 'model_has_permissions',

        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * table should be used to retrieve your models roles. We have chosen a
         * basic default value but you may easily change it to any table you like.
         */

        'model_has_roles' => 'model_has_roles',

        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * table should be used to retrieve your roles permissions. We have chosen a
         * basic default value but you may easily change it to any table you like.
         */

        'role_has_permissions' => 'role_has_permissions',
    ],

    /*
     * By default all permissions will be cached for 24 hours unless a permission or
     * role is updated. Then the cache will be flushed immediately.
     */

    'cache_expiration_time' => 60 * 24,

    /*
     * By default we'll make an entry in the application log when the permissions
     * could not be loaded. Normally this only occurs while installing the packages.
     *
     * If for some reason you want to disable that logging, set this value to false.
     */

    'log_registration_exception' => true,

    /**
     * Pre-defined role and permission
     * 
     */
    'pre_role' => [
        "Pemilik Usaha",
        "Admin Online",
        "Admin Pasar",
        "Petugas Entry",
    ],
    'pre_permission' => [
        "view user" => "bisa melihat daftar customer pengguna aplikasi",
        "edit user" => "bisa menambah, mengubah, menghapus daftar customer pengguna aplikasi",
        "view admin" => "bisa melihat siapa saja yang menjadi admin dari aplikasi",
        "edit admin" => "bisa menambah, mengubah, menghapus admin dari aplikasi",
        "view peran" => "bisa melihat apa saja peranan admin dalam aplikasi",
        "edit peran" => "bisa mengubah, menambah, menghapus peranan admin dalam aplikasi",
        "view produk-kategori" => "bisa melihat produk dan kategori dari aplikasi.",
        "edit produk-kategori" => "bisa mengubah, menambah produk dan kategori dari aplikasi.(kecuali mengurangi stok)",
        "delete produk-kategori" => "menghapus produk dan kategori",
        "view customer" => "bisa melihat daftar dan data customer dari aplikasi",
        "edit customer" => "bisa mengubah data dari customer aplikasi",
        "view grid" => "bisa melihat daftar grid dari customer",
        "edit grid" => "bisa menambah, mengubah dan menghapus grid customer",
        "add order" => "bisa menambah pesanan baru secara offline",
        "view order" => "bisa melihat daftar order dari customer",
        "edit order" => "bisa mengubah data dan status order dari customer, termasuk membuat invoice",
        "view report" => "bisa melihat laporan penjualan",
        "minus stock" => "kuasa khusus untuk mengurangi stok",
        "set payment" => "untuk mengubah status transaksi menjadi lunas"
    ],
    'pre_assignment' => [
        "Pemilik Usaha" => "all access",
        "Admin Online" => [
            "view user","edit user","view admin","view peran","view produk-kategori",
            "edit produk-kategori","view grid","view customer","view order", "edit order",
            "view report"
        ],
        "Admin Pasar" => [
            "view admin","view peran","view produk-kategori","view customer","view grid",
            "add order", "view order", "view report"
        ],
        "Petugas Entry" => [
            "view admin","view peran","edit produk-kategori", "view produk-kategori","view customer",
            "view grid", "view order", "view report"
        ]
    ],
];
