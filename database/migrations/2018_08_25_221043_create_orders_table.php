<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status')->nullable();
            $table->integer('payment')->nullable();
            $table->integer('shipping')->default(0);
            $table->unsignedInteger('user_id')->nullable();
            $table->string('consignee')->nullable();
            $table->string('tracking')->default('');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('order_product', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id')->nullable();
            $table->unsignedInteger('product_id')->nullable();
            $table->integer('qty')->default(0);
            $table->text('detail')->nullable();
            $table->bigInteger('sell_price')->default(0);
            $table->timestamps();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products');
        });
        
        Schema::create('order_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identity');
            $table->string('status');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        if (Schema::hasTable('order_product')) {
            Schema::table('order_product', function (Blueprint $table){
                $table->dropForeign('order_product_order_id_foreign');
                $table->dropForeign('order_product_product_id_foreign');
                $table->dropColumn('order_id');
                $table->dropColumn('product_id');
            });
            Schema::drop('order_product');
        };
        if (Schema::hasTable('orders')) {
            Schema::table('orders', function (Blueprint $table){
                $table->dropForeign('orders_user_id_foreign');
                $table->dropColumn('user_id');
            });
            Schema::drop('orders');
        }
        Schema::drop('order_statuses');
    }
}
