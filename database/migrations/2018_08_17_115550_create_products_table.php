<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku')->unique();
            $table->boolean('is_active')->default(1);
            $table->string('title');
            $table->string('price_base');
            $table->text('description');
            $table->float('weight')->default(0);
            $table->text('stock');
            $table->bigInteger('viewed')->default(0);
            $table->text('images')->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('category_id')->nullable();
            $table->timestamps();
            $table->index('sku');
            $table->index('title');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('products')) {
            Schema::table('products', function (Blueprint $table){
                $table->dropForeign('products_created_by_foreign');
                $table->dropForeign('products_category_id_foreign');
                $table->dropIndex('products_sku_index');
                $table->dropIndex('products_title_index');
                $table->dropColumn('created_by');
                $table->dropColumn('category_id');
                $table->dropColumn('sku');
                $table->dropColumn('title');
            });
            Schema::drop('products');
        }
    }
}
