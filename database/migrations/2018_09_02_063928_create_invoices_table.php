<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('setting');
            $table->text('value');
        });
        
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_number')->nullable();
            $table->unsignedInteger('order_id')->nullable();
            $table->dateTime('payment_date')->nullable();
            $table->timestamps();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('invoices')) {
            Schema::table('invoices', function (Blueprint $table){
                $table->dropForeign('invoices_order_id_foreign');
                $table->dropColumn('order_id');
            });
            Schema::drop('invoices');
        }
        
        Schema::dropIfExists('settings');
        
    }
}
