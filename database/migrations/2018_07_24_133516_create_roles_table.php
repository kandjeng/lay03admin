<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('levels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('progress_type');
            $table->integer('price_progress');
            $table->timestamps();
        }); 

        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(0);
            $table->text('address');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('level_id')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('level_id')->references('id')->on('levels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('customers')) {
            Schema::table('customers', function (Blueprint $table){
                $table->dropForeign('customers_user_id_foreign');
                $table->dropForeign('customers_level_id_foreign');
                $table->dropColumn('user_id');
                $table->dropColumn('level_id');
            });
            Schema::drop('customers');
        }
        Schema::dropIfExists('levels');
    }
}
