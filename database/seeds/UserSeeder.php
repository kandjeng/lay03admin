<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        for ($i=2; $i < 6 ; $i++) { 
            DB::table('levels')->insert([
                "title" => "Grid-".($i-1),
                "progress_type" => "percentage",
                "price_progress" => $i * 5,
                'created_at' => \Carbon\Carbon::now()
            ]);
        };
        DB::table('users')->insert([
            'name' => "Om Lay",
            'email' => "admin@demo.com",
            'phone' => "0811858989",
            'is_admin'=> true,
            'password' => bcrypt('tinkerbell!'),
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('users')->insert([
            'name' => "Admin Online",
            'email' => "adminonline@demo.com",
            'phone' => "089598758216",
            'is_admin'=> true,
            'password' => bcrypt('password'),
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('users')->insert([
            'name' => "Admin Pasar",
            'email' => "adminpasar@demo.com",
            'phone' => "089598758212",
            'is_admin'=> true,
            'password' => bcrypt('password'),
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('users')->insert([
            'name' => "Petugas Entry",
            'email' => "adminentry@demo.com",
            'phone' => "089598758213",
            'is_admin'=> true,
            'password' => bcrypt('password'),
            'created_at' => \Carbon\Carbon::now()
        ]);
    }
}