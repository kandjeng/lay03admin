<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'title' => "UNCATEGORIZED",
            'cover' => "",
            'parent_id' => 0,
            'created_by' => 2,
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('categories')->insert([
            'title' => "Fashion Wanita",
            'cover' => "storage/catpic/2/baju-wanita.jpg",
            'parent_id' => 0,
            'created_by' => 2,
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('categories')->insert([
            'title' => "Fashion Pria",
            'cover' => "storage/catpic/3/fashion-pria.jpg",
            'parent_id' => 0,
            'created_by' => 2,
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('categories')->insert([
            'title' => "Aksesoris Handphone",
            'cover' => "storage/catpic/4/phone-accesorries.jpg",
            'parent_id' => 0,
            'created_by' => 2,
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('categories')->insert([
            'title' => "Lainnya",
            'cover' => "storage/catpic/5/fashion-accessories.jpg",
            'parent_id' => 0,
            'created_by' => 2,
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('categories')->insert([
            'title' => "Setelan Celana Wanita",
            'cover' => "storage/catpic/6/setelan-celana-wanita.jpg",
            'parent_id' => 2,
            'created_by' => 2,
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('categories')->insert([
            'title' => "Setelan Rok Wanita",
            'cover' => "storage/catpic/7/setelan-rok-wanita.jpg",
            'parent_id' => 2,
            'created_by' => 2,
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('categories')->insert([ 
            'title' => "Kemeja Wanita",
            'cover' => "storage/catpic/8/kemeja-wanita.jpg",
            'parent_id' => 2,
            'created_by' => 2,
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('products')->insert([
            'sku' => "SCW0006",
            'title' => "stelan hodie,zipper depan (baby terri)",
            'price_base' => 50000,
            'weight' => 0.5,
            'description' => "Warna tosca, pj baju 65cm, LD106cm, pj cln 91cm, jaket ada saku",
            'stock' => json_encode(collect([["variance"=>"fit to L","stock"=>120,"keep"=>0]])),
            'images' => json_encode(["storage/products/1/6.jpg"]),
            'created_by' => 2,
            'category_id' => 6,
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('products')->insert([
            'sku' =>"SCW0007",
            'title' => "stelan songket zipper depan (supernova mix katun prada)",
            'price_base' => 76000,
            'description' => "pj bj 66cm,LD102cm,pj cln 97cm, abu,dusty pink,cln pinggang karet",
            'stock' => json_encode(collect([["variance"=>"M","stock"=>100,"keep"=>0],["variance"=>"L","stock"=>120,"keep"=>0]])),
            'weight' => 0.31,
            'images' => json_encode(["storage/products/2/7.jpg"]),
            'created_by' => 2,
            'category_id' => 6,
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('products')->insert([
            'sku' =>"SCW0008",
            'title' => "stelan sophia (baby terri)",
            'price_base' => 48000,
            'description' => "pj atasan 65cm,LD100cm,pj cln 90cm, (magenta,coksu)",
            'stock' => json_encode(collect([["variance"=>"fit to L","stock"=>120,"keep"=>0]])),
            'weight' => 0.43,
            'images' => json_encode(["storage/products/3/3.jpg"]),
            'created_by' => 2,
            'category_id' => 6,
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('products')->insert([
            'sku' =>"SCW0009",
            'title' => "stelan capri (baju spandek combi ceruti,cln spandek)",
            'price_base' => 18000,
            'description' => "pj baju 80cm,LD 100cm,pj cln 71cm, putih",
            'stock' => json_encode(collect([["variance"=>"M","stock"=>100,"keep"=>0],["variance"=>"L","stock"=>120,"keep"=>0]])),
            'weight' => 0.29,
            'images' => json_encode(["storage/products/4/4.jpg"]),
            'created_by' => 2,
            'category_id' => 6,
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('products')->insert([
            'sku' =>"SCW0010",
            'title' => "stelan tribal (baby terri)",
            'price_base' => 47000,
            'description' => "pj baju 68cm,LD100cm,pj cln 90cm, navy,abumisty,magenta",
            'stock' => json_encode(collect([["variance"=>"fit to L","stock"=>120,"keep"=>0]])),
            'weight' => 0.45,
            'images' => json_encode(["storage/products/5/5.jpg"]),
            'created_by' => 2,
            'category_id' => 6,
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('products')->insert([
            'sku' =>"SCW0011",
            'title' => "stelan tribal (baby ngepet)",
            'price_base' => 87000,
            'description' => "pj baju 68cm,LD100cm,pj cln 90cm ",
            'stock' => json_encode(collect([["variance"=>"M","stock"=>100,"keep"=>0],["variance"=>"L","stock"=>120,"keep"=>0]])),
            'weight' => 0.43,
            'images' => json_encode(["storage/products/6/5.jpg"]),
            'created_by' => 2,
            'category_id' => 6,
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('products')->insert([
            'sku' =>"SRW0003",
            'title' => "stelan rok pinguin+blouse batwing (spandek)",
            'price_base' => 20000,
            'description' => "pj atasan 62cm,pj rok belakang 94cm,pj rok depan 51cm abu,cream",
            'stock' => json_encode(collect([["variance"=>"fit to L","stock"=>120,"keep"=>0]])),
            'weight' => 0.43,
            'images' => json_encode(["storage/products/7/6.jpg"]),
            'created_by' => 2,
            'category_id' => 7,
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('products')->insert([
            'sku' =>"SRW0004",
            'title' => "stelan rok elsa (spandek)",
            'price_base' => 16000,
            'description' => "pj baju 61cm,LD 96cm,pj rok 45cm, merah",
            'stock' => json_encode(collect([["variance"=>"M","stock"=>100,"keep"=>0],["variance"=>"L","stock"=>120,"keep"=>0]])),
            'weight' => 0.31,
            'images' => json_encode(["storage/products/8/7.jpg"]),
            'created_by' => 2,
            'category_id' => 7,
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('products')->insert([
            'sku' =>"SRW0005",
            'title' => "stelan rok alexa",
            'price_base' => 33000,
            'description' => "pj atasan 67cm,LD100cm,pj rok 45cm, hitam",
            'stock' => json_encode(collect([["variance"=>"fit to L","stock"=>120,"keep"=>0]])),
            'weight' => 0.25,
            'images' => json_encode(["storage/products/9/8.jpg"]),
            'created_by' => 2,
            'category_id' => 7,
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('products')->insert([
            'sku' =>"KW0002",
            'title' => "kemeja rina (katun yandet)",
            'price_base' => 37000,
            'description' => "pj 67cm,LD100cm, coklat, tgn pjng,kancing full hidup",
            'stock' => json_encode(collect([["variance"=>"M","stock"=>100,"keep"=>0],["variance"=>"L","stock"=>120,"keep"=>0]])),
            'weight' => 0.16,
            'images' => json_encode(["storage/products/10/9.jpg"]),
            'created_by' => 2,
            'category_id' => 8,
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('products')->insert([
            'sku' =>"KW0003",
            'title' => "kemeja dress polkadot (katun rayon)",
            'price_base' => 34000,
            'description' => "pj dress 95cm,LD102cm (coklat) tgn pjng,kancing full hidup",
            'stock' => json_encode(collect([["variance"=>"fit to L","stock"=>120,"keep"=>0]])),
            'weight' => 0.21,
            'images' => json_encode(["storage/products/11/7.jpg"]),
            'created_by' => 2,
            'category_id' => 8,
            'created_at' => \Carbon\Carbon::now()
        ]);
        DB::table('products')->insert([
            'sku' =>"KW0004",
            'title' => "kemeja kineta (katun rayon) ",
            'price_base' => 41000,
            'description' => "pj kemeja 75cm,LD102cm, merah,tgn pjng,kancing full hidup,ada saku",
            'stock' => json_encode(collect([["variance"=>"M","stock"=>100,"keep"=>0],["variance"=>"L","stock"=>120,"keep"=>0]])),
            'weight' => 0.71,
            'images' => json_encode(["storage/products/12/11.jpg"]),
            'created_by' => 2,
            'category_id' => 8,
            'created_at' => \Carbon\Carbon::now()
        ]);
    }
}