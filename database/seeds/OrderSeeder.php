<?php

use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('order_statuses')->insert([
            'identity' => "status",
            'status'=> "baru"
        ]);
        DB::table('order_statuses')->insert([
            'identity' => "status",
            'status'=> "diproses"
        ]);
        DB::table('order_statuses')->insert([
            'identity' => "status",
            'status'=> "selesai"
        ]);
        DB::table('order_statuses')->insert([
            'identity' => "status",
            'status'=> "batal"
        ]);
        DB::table('order_statuses')->insert([
            'identity' => "payment",
            'status'=> "pending"
        ]);
        DB::table('order_statuses')->insert([
            'identity' => "payment",
            'status'=> "lunas"
        ]);
        DB::table('order_statuses')->insert([
            'identity' => "payment",
            'status'=> "hutang"
        ]);
    }
}