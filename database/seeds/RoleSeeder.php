<?php 

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions
        $permissions = config('permission.pre_permission');
        foreach ($permissions as $key => $permission) {
            Permission::create(['name' => $key]);    
        }

        // create roles and assign created permissions

        /* $role = Role::create(['name' => 'moderator']);
        $role->givePermissionTo(['publish articles', 'unpublish articles']); */
        $roles = config('permission.pre_role');
        $assignments = config('permission.pre_assignment');
        foreach ($roles as $key => $role) {
            $assign_role = Role::create(['name' => $role]);
            $assign_role->givePermissionTo($assignments[$role]==="all access"?(Permission::all()):$assignments[$role]);
            \App\Models\User::find($key+1)->assignRole($role);    
        }
    }
}