<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'cors', 'prefix' => '/v1'], function () {
    Route::post('/login', 'CustomerController@authenticate');
    Route::post('/register', 'CustomerController@register');
    Route::get('/active/{token}', 'CustomerController@active');
    Route::post('/updateprofile','CustomerController@updateProfile');
    Route::post('/changepassword','CustomerController@changePass');
    Route::get('/logout/{api_token}', 'CustomerController@logout');
    Route::get('/category/top/{id}', 'CommerceController@top');
    Route::post('/category/product','CommerceController@catProduct');
    Route::post('/popularproduct','CommerceController@popProduct');
    Route::post('/product','CommerceController@getProduct');
    Route::post('/placeorder','CommerceController@placeOrder');
    Route::post('/orderhistory','CommerceController@history');
    Route::post('/finish','CommerceController@finishOrder');
    Route::post('/products','CommerceController@products');
    Route::post('/search','CommerceController@search');
});