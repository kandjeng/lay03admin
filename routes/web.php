<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Auth::routes();
Route::get('/slides',function(){
    return view('slides');
});
Route::get('logout', 'Auth\LoginController@logout');
Route::get('/', 'HomeController@index')->name('home');
Route::get('changepass', 'UserController@viewChangePass');
Route::post('changepass', 'UserController@changePass');

Route::prefix('user')->group(function(){
    Route::get('alladmin', 'UserController@allAdmin');
    Route::get('addadmin', 'UserController@viewAdd');
    Route::get('{id}/editadmin', 'UserController@viewEdit')->name('editProfile');
    Route::get('roleadmin', 'UserController@allAdminRole');
    Route::post('editrole','UserController@editRole')->name('editRole');
    Route::post('addrole','UserController@addRole')->name('addRole');
    Route::post('deleterole','UserController@deleteRole')->name('deleteRole');
    Route::post('addadmin', 'UserController@add')->name('addAdmin');
    Route::post('editadmin', 'UserController@edit')->name('editAdmin');
    Route::post('deleteadmin', 'UserController@delete')->name('deleteAdmin');
});
Route::prefix('category')->group(function(){
    Route::get('{category}','CategoryController@category')->name('category');
    Route::get('{category}/add','CategoryController@viewAdd')->name('viewAddCategory');
    Route::get('{category}/edit','CategoryController@viewEdit')->name('viewEditCategory');
    Route::post('add','CategoryController@add')->name('addCategory');
    Route::post('edit','CategoryController@edit')->name('editCategory');
    Route::post('delete','CategoryController@delete')->name('deleteCategory');
});
Route::prefix('product')->group(function(){
    Route::get('all','ProductController@all')->name('products');
    Route::get('{category}/add','ProductController@viewAdd')->name('viewAddProduct');
    Route::get('{product}/edit','ProductController@viewEdit')->name('viewEditProduct');
    Route::post('add','ProductController@add')->name('addProduct');
    Route::post('edit','ProductController@edit')->name('editProduct');
    Route::post('deleteimg','ProductController@deleteImg')->name('deleteImg');
    Route::post('delete','ProductController@delete')->name('deleteProduct');
});
Route::prefix('customer')->group(function(){
    Route::get('all','CustomerLevelController@all')->name('customers');
    Route::get('{user}/edit/','CustomerLevelController@edit')->name('viewEditCustomer');
    Route::post('edit','CustomerLevelController@update')->name('editCustomer');
    Route::post('toggleActive','CustomerLevelController@toggleActive')->name('toggleActive');
    Route::post('setGrid','CustomerLevelController@setGrid')->name('setGrid');
    Route::get('grids','CustomerLevelController@grids')->name('grids');
    Route::get('offlinegrid','CustomerLevelController@offlineGrid')->name('offlineGrid');
    Route::post('updateoffline','CustomerLevelController@updateOffline')->name('updateOfflineGrid');
    Route::get('grid/add/','CustomerLevelController@addGrid')->name('viewAddGrid');
    Route::get('grid/{grid}/edit/','CustomerLevelController@editGrid')->name('viewEditGrid');
    Route::get('grid/{grid}/customer/','CustomerLevelController@gridCustomers')->name('viewGridCustomer');
    Route::post('grid/add','CustomerLevelController@storeGrid')->name('addGrid');
    Route::post('grid/edit','CustomerLevelController@updateGrid')->name('editGrid');
    Route::post('grid/delete','CustomerLevelController@deleteGrid')->name('deleteGrid');
});
Route::prefix('order')->group(function(){
    Route::get('all','OrderController@all')->name('orders');
    Route::get('add','OrderController@add')->name('viewAddOrder');
    Route::get('{order}/edit','OrderController@edit')->name('viewEditOrder');
    Route::post('add','OrderController@store')->name('addOrder');
    Route::post('edit','OrderController@update')->name('editOrder');
    Route::post('delete','OrderController@delete')->name('deleteOrder');
    Route::post('edit/payment','OrderController@setPayment')->name('setPayment');
    Route::post('edit/status','OrderController@setStatus')->name('setStatus');
});
Route::prefix('report')->group(function(){
    Route::get('index','ReportController@index')->name('report');
    Route::get('user/{user}','ReportController@userReport')->name('reportUser');
    Route::get('getOrdersDone','ReportController@getOrdersDone')->name('ordersDone');
    Route::get('getOrdersUser','ReportController@getOrdersUser')->name('ordersUser');
});
Route::prefix('invoice')->group(function(){
    Route::get('all','InvoiceController@index')->name('invoices');
    Route::get('{invoice}','InvoiceController@view')->name('invoice');
    Route::get('pdf/{invoice}','InvoiceController@getPDF')->name('getPDF');
    Route::get('{invoice}/edit','InvoiceController@edit')->name('viewEditInvoice');
    Route::post('edit','InvoiceController@update')->name('editInvoice');
    Route::post('add','InvoiceController@add')->name('addInvoice');
});