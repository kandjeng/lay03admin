<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{ url('/') }}" class="site_title"><!-- <i class="fa fa-lock"></i> --> <img src="/images/logo.png" width="46px" alt="Luck Mas Logo"> <span>Luck Mas Admin</span></a>
        </div>
        
        <div class="clearfix"></div>
        <br />
        
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3></h3>
                <ul class="nav side-menu">
                    <li>
                        <a href="{{route('home')}}">
                            <i class="fa fa-home"></i>
                            Dashboard
                        </a>
                    </li>
                    @can('view produk-kategori')
                    <li><a href="{{route('products')}}"><i class="fa fa-university"></i> Semua Produk</a></li>
                    <li><a><i class="fa fa-sitemap"></i> Produk & Kategori <span class="fa fa-chevron-down"></span></a>
                        {!! render_category_sidebar(get_category_tree()) !!}
                    </li>
                    @endcan
                    <li><a><i class="fa fa-edit"></i> Order <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            @can('view order')
                            <li><a href="{{route('orders')}}">Lihat Semua Order</a></li>
                            <li><a href="{{route('invoices')}}">Faktur/Invoice</a></li>
                            @endcan
                            
                        </ul>
                    </li>
                    @can('view order')
                    <li><a href="{{route('report')}}"><i class="fa fa-tasks"></i> Laporan Penjualan</a></li>
                    @endcan
                    <li><a><i class="fa fa-group"></i> Administrasi <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="/user/alladmin">Daftar Admin</a></li>
                            @can('edit admin')
                            <li><a href="/user/addadmin">Tambah Admin</a></li>
                            @endcan
                            @can('view peran')
                            <li><a href="/user/roleadmin">Peran Admin</a></li>
                            @endcan
                        </ul>
                    </li>
                    <li><a><i class="fa fa-child"></i> Pengguna Aplikasi <span class="fa fa-chevron-down"></a>
                        <ul class="nav child_menu">
                            @can('view customer')
                            <li><a href="{{route('customers')}}">Daftar Pelanggan/Pengguna</a></li>
                            @endcan
                            @can('view grid')
                            <li><a href="{{route('grids')}}">Grid Pelanggan</a></li>
                            @endcan
                            @can('edit customer')
                            <li><a href="{{route('offlineGrid')}}">Grid Penjualan Offline</a></li>
                            @endcan
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
        
        <!-- /menu footer buttons -->

        <!-- HID DUE TO SIMPLICITY -->
        <!-- <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ url('/logout') }}">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div> -->
        <!-- /menu footer buttons -->
    </div>
</div>