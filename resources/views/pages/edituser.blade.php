@extends('layouts.blank')

@push('stylesheets')
	
@endpush

@section('main_container')

    <div class="right_col" role="main">
        <h3>Edit Profile</h3>
        <div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Edit Detail Profil</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
						{!! BootForm::open(['id' =>'addcategory', 'url' => route('editAdmin'), 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
							<input type="hidden" name="current_id" value="{{$user->id}}">
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="user-nama">Nama <span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input id="user-nama" type="text" class="form-control col-md-5 col-xs-12" name="name" value="{{$user->name}}" required>
								</div>
                            </div>
                            @if (\Auth::user()->hasPermissionTo('edit admin') && $user->id != \Auth::user()->id)
                            <div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="user-role">Peran <span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<select id="user-role" name="role" class="form-control cold-md-5 col-xs-12" {!! \Auth::user()->hasPermissionTo('edit admin')?'':'disabled'!!}>
										@foreach (\Spatie\Permission\Models\Role::all() as $idx => $role)
                                            <option value="{{$role['name']}}" {{$user->hasRole($role)?'selected':''}}>
												{!!$role['name']!!}
                                            </option>
										@endforeach
									</select>
								</div>
                            </div>
                            @endif
                            <div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="user-email">Email</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input id="user-email" type="email" class="form-control col-md-5 col-xs-12" name="email" value="{{$user->email}}">
								</div>
                            </div>
                            <div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="user-phone">Phone <span class="required">*</span></label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input id="user-phone" type="phone" class="form-control col-md-5 col-xs-12" name="phone" value="{{$user->phone}}" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="user-pass">Ubah Password ?</label>
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									<input id="user-pass" type="text" class="form-control" name="password">
									<div class="text-right">
										<button id="random-pass" type="button" class="btn btn-medium btn-info"> Password Acak </button>
										<button id="copyto" type="button" class="btn btn-medium"> Copy </button>
									</div>
								</div>
							</div>
                            <div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                    <button type="submit" class="btn btn-success">Simpan Profil</button>
                                    @if (\Auth::user()->hasPermissionTo('edit admin') && $user->id != \Auth::user()->id)
                                    <button type="button" data-toggle="modal" data-target="#modals" class="btn btn-danger btn-delete">Hapus</button>
                                    @endif
								</div>
							</div>
						{!! BootForm::close() !!}
					</div>
				</div>
			</div>
		</div>
    </div>
    @if (\Auth::user()->hasPermissionTo('edit admin') && $user->id != \Auth::user()->id)
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">
		{!! BootForm::open(['url' => route('deleteAdmin'), 'method' => 'post']) !!}
			<div class="modal-dialog modal-sm">
    			<div class="modal-content">
	    			<div class="modal-header">
	    				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	    					<span aria-hidden="true">×</span>
	    				</button>
	    				<h4 class="modal-title">Hapus Admin</h4>
	    			</div>
	    			<div class="modal-body">
	    				<input type="hidden" name="current-id" value="{{$user->id}}">
	    				<p class="text-center">Apa Anda yakin</p>
	    			</div>
	    			<div class="modal-footer">
	    				<a class="btn btn-default" data-dismiss="modal">Batal</a>
	    				<button type="submit" class="btn btn-danger">Hapus Admin</button>
	    			</div>
	    		</div>
	    	</div>
	    {!! BootForm::close() !!}
    </div>
    @endif
@endsection
@push('scripts')
	<script type="text/javascript">
		$('#random-pass').click(function() {
			var length = 10,
				charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
				retVal = "";
			for (var i = 0, n = charset.length; i < length; ++i) {
				retVal += charset.charAt(Math.floor(Math.random() * n));
			}
			$('#user-pass').val(retVal);
		});
		$('#copyto').click(function() {
			var $temp = $("<input>");
			$("body").append($temp);
			$temp.val($('#user-pass').val()).select();
			document.execCommand("copy");
			$temp.remove();
		});
	</script>
@endpush