@extends('layouts.blank')

@push('stylesheets')
	
@endpush

@section('main_container')

    <div class="right_col" role="main">
		<div class="preloader"><i class="fa fa-power-off"></i></div>
        <h3>Peranan Admin</h3>
        <div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Detail Peranan Admin</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">

						<div class="col-sm-3 col-xs-4">
							<ul class="nav nav-tabs tabs-left">
								@foreach ($roles as $key => $role)
								<li{!!($key==0)?' class="active"':''!!}>
									<a href="#role-{!!$role->id!!}" data-toggle="tab" aria-expanded="{!!($key==0)?'true':'false'!!}">{{$role->name}}</a>
								</li>
								@endforeach
								@can("edit peran")
								<li>
									<a href="#role-new" data-toggle="tab" aria-expanded="false"><i class="fa fa-plus"> </i> Tambah Baru</a>
								</li>
								@endcan
							</ul>
						</div>
	
						<div class="col-sm-9 col-xs-8">
							<div class="tab-content">
								@foreach ($roles as $key => $role)
								<div class="tab-pane{!!($key==0)?' active':''!!}" id="role-{!!$role->id!!}">
									<p class="lead">Peranan : {{$role->name}}</p>
									<hr>
									@can("edit peran")
									<form action="" method="POST" id="roleform-{{$role->id}}" class="well well-sm" style="background:#fdfdfd" role-status="exist">
										<input type="hidden" name="role_id" value="{!!$role->id!!}">
										<ul class="to_do">
											@foreach($pre_perms as $key_perm => $pre_perm)
											<li>
												<div class="icheck-info">
												<input id="cb{{$key}}-{{$key_perm}}" type="checkbox" class="permcheck" name="permissions[]" value="{!!$key_perm!!}" {{$role->hasPermissionTo($key_perm)?'checked':''}}> 
												<label for="cb{{$key}}-{{$key_perm}}">
													<b>{{$key_perm}} : </b>{{$pre_perm}}
												</label>
												</div>
											</li>
											@endforeach
										</ul>
										<button type="submit" class="btn btn-success">Simpan</button>
										<button type="button" class="btn btn-danger btn-del" role-name="{{$role->name}}" role-id="{{$role->id}}">Hapus Peran</button>
									</form>
									@else
										@foreach ($role->permissions as $permission)
										<p style="border-bottom:solid 1px #ddd;padding:5px 0;">
											<b>{{$permission->name}}&emsp;</b>: {{$pre_perms[$permission->name]}}
										</p>
										@endforeach
									@endcan
								</div>
								@endforeach
								@can('edit peran')
								<div class="tab-pane" id="role-new">
									<p class="lead">Buat Peran Baru</p>
									<hr>
									<form action="{{route('addRole')}}" method="POST" id="roleform-new" class="well well-sm" style="background:#fdfdfd" role-status="new">
										<ul class="to_do">
											{{ csrf_field() }}
											<li><input type="text" name="role_name" placeholder="Nama Peran" class="form-control"></li>
											@foreach($pre_perms as $key_perm => $pre_perm)
											<li>
												<div class="icheck-info">
												<input id="cbnew-{{$key_perm}}" type="checkbox" class="permcheck" name="permissions[]" value="{!!$key_perm!!}"> 
												<label for="cbnew-{{$key_perm}}">
													<b>{{$key_perm}} : </b>{{$pre_perm}}
												</label>
												</div>
											</li>
											@endforeach
										</ul>
										<button type="submit" class="btn btn-success">Tambahkan Peran</button>
									</form>
								</div>
								@endcan
							</div>
						</div>
	
						<div class="clearfix"></div>
	
					  </div>
				</div>
			</div>
		</div>
    </div>
    @can('edit peran')
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">
		{!! BootForm::open(['url' => route('deleteRole'), 'method' => 'post']) !!}
			<div class="modal-dialog modal-sm">
    			<div class="modal-content">
	    			<div class="modal-header">
	    				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	    					<span aria-hidden="true">×</span>
	    				</button>
	    				<h4 class="modal-title">Hapus Peran</h4>
	    			</div>
	    			<div class="modal-body">
	    				<input id="delrole_id" type="hidden" name="current_id" value="0">
	    				<p class="text-center">Peran akan dihapus: </p>
	    			</div>
	    			<div class="modal-footer">
	    				<a class="btn btn-default" data-dismiss="modal">Batal</a>
	    				<button type="submit" class="btn btn-danger">Hapus Peran</button>
	    			</div>
	    		</div>
	    	</div>
	    {!! BootForm::close() !!}
    </div>
    @endcan
@endsection
@push('scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
		});
		$('form.well').submit(function(e){
			if ($(this).attr('role-status')=="exist"){
				e.preventDefault();
				$('.preloader').show(150);
				var data= $(this).serializeArray();
				$.post('{{ route('editRole') }}', data, function(response){
					$('.preloader').hide();	
					new PNotify({
						title: response.title,
						text: response.text,
						type: response.type,
						styling : 'bootstrap3'
					});
				},'json');
			}
		});
		$('.btn-del').click(function(){
			$('#delrole_id').attr('value',$(this).attr('role-id'));
			$('.modal-body p').append("<b>"+$(this).attr('role-name')+"</b>")
			$('#modals').modal('show');
		});
	</script>
@endpush