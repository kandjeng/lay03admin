@extends('layouts.blank')

@push('stylesheets')
	
@endpush

@section('main_container')

    <div class="right_col" role="main">
        <h3>Tambah Produk</h3>
        <div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Tambah Deskripsi Produk Baru</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
						{!! BootForm::open(['id' =>'addproduct', 'url' => route('addProduct'), 'method' => 'post', 'class' => 'form-horizontal form-label-left','enctype'=>"multipart/form-data"]) !!}
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prod-kateg">Kategori <span class="required">*</span></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
                                    <select id="prod-kateg" class="form-control cold-md-5 col-xs-12" name="category" required {{($cat_id>1)?'disabled':''}}>
                                        @foreach ($category_tree as $category)
                                            @if ($category['id']>1)
                                            <option value="{{$category['id']}}" class="{{$category['top']}}" {{($cat_id==$category['id'])?'selected':''}}>
                                                {!!$category['title']!!}
                                            </option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="parent" value="{{$cat_id}}">
                                    @if ($cat_id>2)
                                    <input type="hidden" name="category" value="{{$cat_id}}">
                                    @endif
                                </div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prod-nama">Nama Produk <span class="required">*</span></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<input id="prod-nama" type="text" class="form-control col-md-5 col-xs-12" name="title" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prod-sku">SKU Produk <span class="required">*</span></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<input id="prod-sku" type="text" class="form-control col-md-5 col-xs-12" name="sku" required>
								</div>
                            </div>
                            <div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prod-price">Harga Dasar <span class="required">*</span></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<input id="prod-price" placeholder="Rp." type="text" class="form-control col-md-5 col-xs-12" name="price_base" required>
								</div>
                            </div>
                            <div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prod-weight">Berat Produk <span class="required">*</span></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<input id="prod-weight" type="text" class="form-control col-md-5 col-xs-12" name="weight" placeholder="dalam kg, contoh: 0.200" required>
								</div>
                            </div>
                            <div id="stockgroup" class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prod-stock">Stok & variasi <span class="required">*</span></label>
								<div class="row col-md-8 col-sm-9 col-xs-12">
									<div class="row-group-0">
										<div class="col-md-5 col-sm-5 col-xs-12">
											<input type="text" class="mustfill form-control col-md-5 col-xs-12" name="stock[0][variance]" placeholder="Variasi Produk" required>
										</div>
										<div class="col-md-2 col-sm-2 col-xs-3">
											<input type="number" class="mustfill form-control col-md-5 col-xs-12" name="stock[0][stock]" placeholder="Stock" required>
										</div>
										<div class="col-md-2 col-sm-2 col-xs-3">
											<input type="number" class="form-control col-md-5 col-xs-12 keeps" name="stock[0][keep]" placeholder="Keep Stock" required>
										</div>
										<div class="col-md-3 col-sm-3 col-xs-6 btn-action-group">
											<button data-next="1" type="button" class="btn btn-primary btn-addstock"><i class="fa fa-plus"></i></button>
										</div>
									</div>
								</div>
								
                            </div>
                            <div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prod-desc">Deskripsi Produk <span class="required">*</span></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
                                    <textarea name="description" id="prod-desc" rows="8" placeholder="deskripsi dari produk" class="form-control" required></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prod-image">Gambar Produk <span class="required">*</span></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<input id="prod-image" type="file" class="filestyle" data-buttonName="btn-primary" data-buttonText="tambah gambar" name="image[]" accept="image/*" required multiple>
								</div>
							</div>
                            <div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2 col-sm-offset-2">
                                    <button type="submit" class="btn btn-success">Tambahkan Produk</button>
								</div>
							</div>
						{!! BootForm::close() !!}
					</div>
				</div>
			</div>
		</div>
    </div>
    
@endsection
@push('scripts')
	<script type="text/javascript">
		$('#stockgroup').on('click','.btn-addstock',function(){
			var isFilled=true;
			var next= $(this).attr('data-next');
			$('input.mustfill').each(function(){
				if (!$(this).val()){
					isFilled=false;
				};
			});
			if (isFilled){
				if (!$('#stockgroup input.keeps').last().val()){
					$('#stockgroup input.keeps').last().val(0);
				};
				$('#stockgroup .btn-action-group .btn-remstock').last().remove();
				$('#stockgroup .row').append('<div class="row-group-'+next+'"><div class="col-md-5 col-sm-5 col-xs-12"><input type="text" class="mustfill form-control col-md-5 col-xs-12" name="stock['+next+'][variance]" placeholder="Variasi Produk" required></div><div class="col-md-2 col-sm-2 col-xs-3"><input type="number" class="mustfill form-control col-md-5 col-xs-12" name="stock['+next+'][stock]" placeholder="Stock" required></div><div class="col-md-2 col-sm-2 col-xs-3"><input type="number" class="form-control col-md-5 col-xs-12" name="stock['+next+'][keep]" placeholder="Keep Stock"></div><div class="col-md-3 col-sm-3 col-xs-6 btn-action-group"><button data-next="'+(parseInt(next)+1)+'" type="button" class="btn btn-primary btn-addstock"><i class="fa fa-plus"></i></button><button data-rem="'+next+'" type="button" class="btn btn-sm btn-danger btn-remstock"><i class="fa fa-minus"></i></button></div></div>');
				$(this).remove();
			};
		})
		$('#stockgroup').on('click','.btn-remstock',function(){
			var row = $(this).attr('data-rem');
			$('.row-group-'+row).remove();
			if (parseInt(row)>1) {
				$('#stockgroup .btn-action-group').last().append('<button data-next="'+row+'" type="button" class="btn btn-primary btn-addstock"><i class="fa fa-plus"></i></button><button data-rem="'+(parseInt(row)-1)+'" type="button" class="btn btn-sm btn-danger btn-remstock"><i class="fa fa-minus"></i></button>');
			} else {
				$('#stockgroup .btn-action-group').last().append('<button data-next="'+row+'" type="button" class="btn btn-primary btn-addstock"><i class="fa fa-plus"></i></button>');
			}
		})
		$('#addproduct').submit(function(event){
			
		});
	</script>
@endpush