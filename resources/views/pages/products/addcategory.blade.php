@extends('layouts.blank')

@push('stylesheets')
	
@endpush

@section('main_container')

    <div class="right_col" role="main">
        <h3>Tambah Kategori</h3>
        <div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Tambah Kategori Baru</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
						{!! BootForm::open(['id' =>'addcategory', 'url' => route('addCategory'), 'method' => 'post', 'class' => 'form-horizontal form-label-left','enctype'=>"multipart/form-data"]) !!}
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="kateg-nama">Nama Kategori <span class="required">*</span></label>
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									<input id="kateg-nama" type="text" class="form-control col-md-5 col-xs-12" name="title" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="kateg-parent">Kategori Induk <span class="required">*</span></label>
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <select id="kateg-parent" class="form-control cold-md-5 col-xs-12" required disabled>
                                        @foreach ($category_tree as $category)
                                            <option value="{{$category['id']}}" class="{{$category['top']}}" {{($category['id']==$current)?'selected':''}}>
                                                {!!$category['title']!!}
                                            </option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="parent_id" value="{{$current}}">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="kateg-cover">Gambar Kategori <span class="required">*</span></label>
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									<input id="kateg-cover" type="file" class="filestyle" data-buttonName="btn-primary" data-buttonText="tambah gambar" name="cover" accept="image/*" required>
								</div>
							</div>
                            <div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                    <button type="submit" class="btn btn-success">Tambahkan Kategori</button>
								</div>
							</div>
						{!! BootForm::close() !!}
					</div>
				</div>
			</div>
		</div>
    </div>
    
@endsection
@push('scripts')
	<script type="text/javascript">
		
	</script>
@endpush