@extends('layouts.blank')

@push('stylesheets')
	<!-- DataTables -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css">
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
    	<div id="cat-op" class="pull-right">
    	@can('edit produk-kategori')
    		<a href="{{route('viewEditCategory',$current)}}" class="btn btn-info"><i class="glyphicon glyphicon-edit"></i> Edit Kategori</a>
    	@endcan
    	@can('delete produk-kategori')
    		<button data-name="{!! $current->title !!}" data-delete="{!!$current->id!!}" class="btn-delete btn btn-danger"><i class="glyphicon glyphicon-trash"></i> Hapus</a>
        @endcan
        </div>
    	<h3>Kategori : {{$current->title}}</h3>
    	<div class="row">
			<div class="col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Sub-Kategori</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						@if (count($categories)>0)
						<table class="table table-striped cattable" id="cat-table">
							<thead>
								<tr>
									<th colspan="2" >subkategori</th>
									<th>produk</th>
									<th>dibuat</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach ($categories as $subcategory)
									<tr>
										<td><a href="{{ route('category',$subcategory) }}" class="tablink"><i class="glyphicon glyphicon-folder-open"</i></a></td>
										<td><a href="{{ route('category',$subcategory) }}" class="tablink">{{ $subcategory->title }}</a></td>
										<td>{{ $subcategory->products->count() }}</td>
										<td>{{ $subcategory->created_at->diffForHumans() }}</td>
										<td>
											<a href="{{route('viewEditCategory',$subcategory)}}" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> edit</a>
											<button class="btn btn-delete btn-danger btn-xs" data-name="{!! $subcategory->title !!}" data-delete="{!! $subcategory->id !!}"><i class="fa fa-trash-o"></i> Hapus</a>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
                        @endif
                        @can('edit produk-kategori')
                        <a href="{{route('viewAddCategory',$current)}}" class="btn btn-dark pull-right"><i class="fa fa-plus"></i> Tambah Sub-Kategori</a>
                        @endcan
					</div>
				</div>
			</div>
		</div>
		<div class="row">
				<div class="col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<h2>Produk dalam kategori</h2>
							<ul class="nav navbar-right panel_toolbox">
								<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
							</ul>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<br>
							<div id="products" class="data-journal">
								{!! $dataTable->table(['class' => 'table dt-responsive display', 'width' => '100%', 'cellspacing' => '0']) !!}
							</div>
							<a href="{{route('viewAddProduct',["id" => $current->id])}}" id="addData" class="btn btn-dark pull-right"><i class="fa fa-plus"></i> Tambah Produk di kategori</a>
						</div>
					</div>
				</div>
			</div>
    </div>
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">
    	
    </div>
    <!-- /page content -->
@endsection
@push('scripts')
	<!-- Jquery DataTables Script -->
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript">
		$('#cat-table, #cat-op').on('click','.btn-delete',function(e){
			e.preventDefault(e);
			$('#modals').html(deleteCategory($(this).attr('data-delete'),$(this).attr('data-name')));
			$('#modals').modal('show');
		})
		function deleteCategory(id,name){
			return '{!! BootForm::open(['url' => route('deleteCategory'), 'method' => 'post']) !!}<div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Hapus: '+name+'</h4></div><div class="modal-body"><input type="hidden" name="cat_id" value="'+ id +'"><p class="text-center">Apa anda yakin ingin menghapus kategori ini?<br></p></div><div class="modal-footer"><a class="btn btn-default" data-dismiss="modal">batal</a><button type="submit" class="btn btn-danger">Hapus</button></div></div></div>{!! BootForm::close() !!}';
		}
		function getModalBig(id){
            return '{!! BootForm::open(['id' =>'addData', 'url' => "route('addData')", 'method' => 'post', 'class' => 'form-horizontal form-label-left','enctype'=>"multipart/form-data"]) !!}<div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button><h4 class="modal-title" id="myModalLabel">$currentcategory->title <i class="fa fa-angle-double-right"></i> Add Data</h4></div><div class="modal-body row"><input type="hidden" name="cat_id" value="'+ id +'"><div class="col-md-8 col-xs-12 col-md-offset-2"><div class="form-group"><label class="control-label" for="data-title">Title</label><input id="data-title" type="text" class="form-control" name="title" required></div><div class="form-group"><label class="control-label" for="data-tags">Data table tags</label><input id="data-tags" type="text" class="tags form-control" name="data_tags" required></div><div class="form-group"><label class="control-label" for="data-file">Data File (in xls, xlsx, csv)</label><input id="data-file" type="file" class="form-control" name="datafile" accept=".xls, .xlsx, .csv" required></div><div class="form-group"><label class="control-label" for="data-desc">Data Short Description</label><textarea id="data-desc" class="form-control" name="short" required></textarea></div></div></div><div class="modal-footer"><a class="btn btn-default" data-dismiss="modal">Close</a><button type="submit" class="btn btn-primary">Save Data</button></div></div></div>{!! BootForm::close() !!}';
        }
	</script>
	{!! $dataTable->scripts() !!}
@endpush