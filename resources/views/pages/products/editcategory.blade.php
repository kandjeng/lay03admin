@extends('layouts.blank')

@push('stylesheets')
	
@endpush

@section('main_container')

    <div class="right_col" role="main">
        <h3>Edit Kategori</h3>
        <div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Edit/ubah Kategori</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
                        {!! BootForm::open(['id' =>'addcategory', 'url' => route('editCategory'), 'method' => 'post', 'class' => 'form-horizontal form-label-left','enctype'=>"multipart/form-data"]) !!}
                            <input type="hidden" name="current_id" value="{{$current->id}}">
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="kateg-nama">Nama Kategori <span class="required">*</span></label>
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									<input id="kateg-nama" type="text" class="form-control col-md-5 col-xs-12" name="title" value="{{$current->title}}" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="kateg-parent">Kategori Induk <span class="required">*</span></label>
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <select id="kateg-parent" name="parent_id" class="form-control cold-md-5 col-xs-12" required>
                                        @foreach ($category_tree as $category)
                                            <option value="{{$category['id']}}" class="{{$category['top']}}" {{($current->parent_id > 0)?(($category['id']==$current->parent->id)?'selected':''):''}}>
                                                {!!$category['title']!!}
                                            </option>
                                        @endforeach
                                    </select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="cover-file">Gambar Kategori <span class="required">*</span></label>
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <img id="cover-preview" src="{{asset(pathinfo($current->cover,PATHINFO_DIRNAME)."/thumb/".pathinfo($current->cover,PATHINFO_BASENAME))}}" alt="cover thumbnail" style="width:50%">
                                    <input id="kateg-cover" type="file" class="filestyle" data-buttonName="btn-primary" data-buttonText="ganti gambar" name="cover" accept="image/*">
								</div>
							</div>
                            <div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                    <button type="submit" class="btn btn-success">Simpan</button>
								</div>
							</div>
						{!! BootForm::close() !!}
					</div>
				</div>
			</div>
		</div>
    </div>
    
@endsection
@push('scripts')
	<script type="text/javascript">
        $('#cover-file').change(function(){
            if ($(this).val()==null || $(this).val()==''){
                $('#cover-preview').show();
            } else {
                $('#cover-preview').hide();
            }
        })
	</script>
@endpush