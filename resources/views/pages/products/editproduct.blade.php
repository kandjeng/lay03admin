@extends('layouts.blank')

@push('stylesheets')

		

@endpush

@section('main_container')

    <div class="right_col" role="main">
		<div class="preloader"><i class="fa fa-power-off"></i></div>
        <h3>Edit Produk</h3>
        <div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Edit Deskripsi Produk</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
						{!! BootForm::open(['id' =>'addproduct', 'url' => route('editProduct'), 'method' => 'post', 'class' => 'form-horizontal form-label-left','enctype'=>"multipart/form-data"]) !!}
							<input type="hidden" name="current_id" value="{{$product->id}}">
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prod-kateg">Kategori <span class="required">*</span></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
                                    <select id="prod-kateg" class="form-control cold-md-5 col-xs-12" name="category" required>
                                        @foreach ($category_tree as $category)
                                            @if ($category['id']>1)
                                            <option value="{{$category['id']}}" class="{{$category['top']}}" {{($product->category_id==$category['id'])?'selected':''}}>
                                                {!!$category['title']!!}
                                            </option>
                                            @endif
                                        @endforeach
									</select>
                                </div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prod-nama">Nama Produk <span class="required">*</span></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<input id="prod-nama" type="text" class="form-control col-md-5 col-xs-12" name="title" value="{{$product->title}}" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prod-sku">SKU Produk <span class="required">*</span></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<input id="prod-sku" type="text" class="form-control col-md-5 col-xs-12" name="sku" value="{{$product->sku}}" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prod-active">Produk Aktif <span class="required">*</span></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
                                    <select id="prod-kateg" class="form-control cold-md-5 col-xs-12" name="is_active" required>
										<option value="1" {{($product->is_active)?'selected':''}}>Aktif</option>
										<option value="0" {{($product->is_active)?'':'selected'}}>Tidak Aktif</option>
									</select>
                                </div>
							</div>
                            <div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prod-price">Harga Dasar <span class="required">*</span></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<input id="prod-price" placeholder="Rp." type="text" class="form-control col-md-5 col-xs-12" name="price_base" value="{{$product->price_base}}" required>
								</div>
                            </div>
                            <div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prod-weight">Berat Produk <span class="required">*</span></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<input id="prod-weight" type="text" class="form-control col-md-5 col-xs-12" name="weight" placeholder="dalam kg, contoh: 0.200" value="{{$product->weight}}" required>
								</div>
                            </div>
                            <div id="stockgroup" class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prod-stock">Stok & variasi <span class="required">*</span></label>
								<div class="row col-md-8 col-sm-9 col-xs-12">
									@foreach (json_decode($product->stock,true) as $key=>$stock)
									<div class="row-group-{!!$key!!}">
										<div class="col-md-5 col-sm-5 col-xs-12">
											<input type="text" class="mustfill form-control col-md-5 col-xs-12" name="stock[{!!$key!!}][variance]" placeholder="Variasi Produk" value="{{$stock['variance']}}" required>
										</div>
										<div class="col-md-2 col-sm-2 col-xs-3">
											<input type="number" class="mustfill form-control col-md-5 col-xs-12" name="stock[{!!$key!!}][stock]" placeholder="Stock" value="{{$stock['stock']}}" required>
										</div>
										<div class="col-md-2 col-sm-2 col-xs-3">
											<input type="number" class="form-control col-md-5 col-xs-12 keeps" name="stock[{!!$key!!}][keep]" placeholder="Keep Stock" value="{{$stock['keep']}}">
										</div>
										<div class="col-md-3 col-sm-3 col-xs-6 btn-action-group">
											@if ($loop->last)
											<button data-next="{{$key+1}}" type="button" class="btn btn-sm btn-primary btn-addstock"><i class="fa fa-plus"></i></button>
												@if ($key>0)
												<button data-rem="{{$key}}" type="button" class="btn btn-sm btn-danger btn-remstock"><i class="fa fa-minus"></i></button>
												@endif
											@endif
										</div>
									</div>
									@endforeach
								</div>
                            </div>
                            <div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prod-desc">Deskripsi Produk <span class="required">*</span></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
                                    <textarea name="description" id="prod-desc" rows="8" placeholder="deskripsi dari produk" class="form-control" required>{{$product->description}}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prod-image">Gambar Produk <span class="required">*</span></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<div id="image-container">
										@foreach (json_decode($product->images) as $key => $image)
										<div class="image-box col-md-4 col-xs-6">
											<img id="cover-preview" src="{{asset(pathinfo($image,PATHINFO_DIRNAME)."/thumb/".pathinfo($image,PATHINFO_BASENAME))}}" alt="cover thumbnail" style="width:100%;">
											<button type="button" class="btn btn-imgdel btn-xs btn-danger" data-imgidx="{{$key}}">&times; hapus</button>
										</div>
										@endforeach
									</div>
									<input id="prod-image" type="file" class="filestyle" data-buttonName="btn-primary" data-buttonText="tambah gambar" name="image[]" accept="image/*" multiple>
                                    
								</div>
							</div>
                            <div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
									<button type="submit" class="btn btn-success">Simpan</button>
								@can('delete produk-kategori')
									<button type="button" class="btn btn-proddel btn-danger">Hapus produk</button>
								@endcan
								</div>
							</div>
						{!! BootForm::close() !!}
					</div>
				</div>
			</div>
		</div>
    </div>
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">
		
	</div>
@endsection
@push('scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
		});
		$('#image-container').on('click','.btn-imgdel',function(e){
			$('#img-index').val();
			$('#modals').html('<form id="deleteImageForm" action="" method="POST"><div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Hapus Gambar</h4></div><div class="modal-body"><input type="hidden" name="product_id" value="{{$product->id}}"><input id="img-index" type="hidden" name="img_idx" value="'+$(this).attr('data-imgidx')+'"><p class="text-center">Hapus gambar dari produk ?</p></div><div class="modal-footer"><a class="btn btn-default" data-dismiss="modal">Batal</a><button type="submit" class="btn btn-danger">Hapus Gambar</button></div></div></div></form>').modal('show');
		});
		$('#modals').on('submit','#deleteImageForm',function(e){
			e.preventDefault();
			$('#modals').modal('hide');
			$('.preloader').show(150);
			var data= $(this).serializeArray();
			$.post('{{ route('deleteImg') }}', data, function(response){
				$('.preloader').hide();	
				new PNotify({
					title: response.title,
					text: response.text,
					type: response.type,
					styling : 'bootstrap3'
				});
				if ('imgs' in response){
					$('#image-container').html("");
					$.each(response.imgs,function(i, val){
						$("#image-container").append('<div class="image-box col-md-4 col-xs-6"><img id="cover-preview" src="/'+val.replace(/\/[^\/]*$/, '')+"/thumb/"+val.replace( /.*\//, '' )+'"><button type="button" class="btn btn-imgdel btn-xs btn-danger" data-imgidx="'+i+'">&times; hapus</button></div>');
					});
				}
			},'json');
		});
		$('.btn-proddel').click(function(){
			$('#modals').html('{!! BootForm::open(['url' => route('deleteProduct'), 'method' => 'post']) !!}<div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close">    <span aria-hidden="true">×</span></button><h4 class="modal-title">HAPUS PRODUK</h4></div><div class="modal-body"><input type="hidden" name="product_id" value="{{$product->id}}"><p class="text-center">Apa Anda yakin ingin menghapus produk ini?</p></div><div class="modal-footer"><a class="btn btn-default" data-dismiss="modal">Batal</a><button type="submit" class="btn btn-danger">Hapus Produk</button></div></div></div></form>{!! BootForm::close() !!}').modal('show');
			
		})
		$('#stockgroup').on('click','.btn-addstock',function(){
			var isFilled=true;
			var next= $(this).attr('data-next');
			$('input.mustfill').each(function(){
				if (!$(this).val()){
					isFilled=false;
				};
			});
			if (isFilled){
				if (!$('#stockgroup input.keeps').last().val()){
					$('#stockgroup input.keeps').last().val(0);
				};
				$('#stockgroup .btn-action-group .btn-remstock').last().remove();
				$('#stockgroup .row').append('<div class="row-group-'+next+'"><div class="col-md-5 col-sm-5 col-xs-12"><input type="text" class="mustfill form-control col-md-5 col-xs-12" name="stock['+next+'][variance]" placeholder="Variasi Produk" required></div><div class="col-md-2 col-sm-2 col-xs-3"><input type="number" class="mustfill form-control col-md-5 col-xs-12" name="stock['+next+'][stock]" placeholder="Stock" required></div><div class="col-md-2 col-sm-2 col-xs-3"><input type="number" class="form-control col-md-5 col-xs-12" name="stock['+next+'][keep]" placeholder="Keep Stock"></div><div class="col-md-3 col-sm-3 col-xs-6 btn-action-group"><button data-next="'+(parseInt(next)+1)+'" type="button" class="btn btn-primary btn-addstock"><i class="fa fa-plus"></i></button><button data-rem="'+next+'" type="button" class="btn btn-sm btn-danger btn-remstock"><i class="fa fa-minus"></i></button></div></div>');
				$(this).remove();
			};
		})
		$('#stockgroup').on('click','.btn-remstock',function(){
			var row = $(this).attr('data-rem');
			$('.row-group-'+row).remove();
			if (parseInt(row)>1) {
				$('#stockgroup .btn-action-group').last().append('<button data-next="'+row+'" type="button" class="btn btn-primary btn-addstock"><i class="fa fa-plus"></i></button><button data-rem="'+(parseInt(row)-1)+'" type="button" class="btn btn-sm btn-danger btn-remstock"><i class="fa fa-minus"></i></button>');
			} else {
				$('#stockgroup .btn-action-group').last().append('<button data-next="'+row+'" type="button" class="btn btn-primary btn-addstock"><i class="fa fa-plus"></i></button>');
			}
		})
	</script>
@endpush