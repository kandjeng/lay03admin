@extends('layouts.blank')

@push('stylesheets')
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css">
    
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
    	<h3>Produk</h3>
    	<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Semua Produk yang terdaftar</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
                        @if (\Auth::user()->hasPermissionTo('edit admin'))
                        <div style="margin-top:2rem">
                            <a href="/product/1/add" id="addNew" class="btn btn-dark"><i class="fa fa-plus"></i> Tambah Produk Baru</a>
                        </div>
                        @endif
                        <br>
                        <div id="products" class="data-journal">
                            {!! $dataTable->table(['class' => 'table dt-responsive display', 'width' => '100%', 'cellspacing' => '0']) !!}
                        </div>
                        <div class="clearfix"></div>
                        
					</div>
				</div>
			</div>
		</div>
    </div>
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">
    	
    </div>
    <!-- /page content -->
@endsection

@push ('scripts')
    <!-- Jquery DataTables Script -->
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/scroller/1.5.1/js/dataTables.scroller.min.js" type="text/javascript"></script>
    {!! $dataTable->scripts() !!}
@endpush