@extends('layouts.blank')

@push('stylesheets')
	
@endpush

@section('main_container')

    <div class="right_col" role="main">
        <h3>Edit Pelanggan</h3>
        <div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Edit/ubah Data Pelanggan</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
                        {!! BootForm::open(['id' =>'editCustomer', 'url' => route('editCustomer'), 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                            <input type="hidden" name="current_id" value="{{$cust['id']}}">
							<div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="customer-aktif">Status Pelanggan <span class="required">*</span></label>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <select id="customer-aktif" name="active" class="form-control cold-md-5 col-xs-12" required>
                                        <option value="1" {{($cust['active'])?'selected':''}}>Aktiv</option>
                                        <option value="0" {{($cust['active'])?'':'selected'}}>nonaktif</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="customer-grid">Grid Pelanggan <span class="required">*</span></label>
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                        <select id="customer-grid" name="grid" class="form-control cold-md-5 col-xs-12" required>
                                            @foreach (\App\Models\Level::all() as $grid)
                                            <option value="{{$grid->id}}" {{($cust['grid']['id']==$grid->id)?'selected':''}}>{{$grid->title}}: {{$grid->price_progress}} {{($grid->progress_type=='flat')?' (Rp.)':'(%)'}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            <div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="customer-nama">Nama Pelanggan <span class="required">*</span></label>
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									<input id="customer-nama" type="text" class="form-control col-md-5 col-xs-12" name="name" value="{{$cust['name']}}" required>
								</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="customer-phone">No Handphone <span class="required">*</span></label>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <input id="customer-phone" type="text" class="form-control col-md-5 col-xs-12" name="phone" value="{{$cust['phone']}}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="customer-email">Email <span class="required">*</span></label>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <input id="customer-email" type="text" class="form-control col-md-5 col-xs-12" name="email" value="{{$cust['email']}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="customer-address">Alamat <span class="required">*</span></label>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <textarea name="address" id="customer-address" rows="8" style="width:100%" required>{{$cust['address']}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="user-pass">Ubah Password ?</label>
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									<input id="user-pass" type="text" class="form-control" name="password">
									<div class="text-right">
										<button id="random-pass" type="button" class="btn btn-medium btn-info"> Password Acak </button>
										<button id="copyto" type="button" class="btn btn-medium"> Copy </button>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                    <button type="submit" class="btn btn-success">Simpan</button>
								</div>
							</div>
						{!! BootForm::close() !!}
					</div>
				</div>
			</div>
		</div>
    </div>
    
@endsection
@push('scripts')
	<script type="text/javascript">
        
	</script>
@endpush