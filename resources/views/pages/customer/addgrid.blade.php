@extends('layouts.blank')

@push('stylesheets')
	
@endpush

@section('main_container')

    <div class="right_col" role="main">
        <h3>Tambah Grid</h3>
        <div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Tambah Grid baru</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
                        {!! BootForm::open(['id' =>'addGrid', 'url' => route('addGrid'), 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="grid-name">Nama Grid <span class="required">*</span></label>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <input id="grid-name" type="text" class="form-control col-md-5 col-xs-12" name="title" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="grid-type">Progresi <span class="required">*</span></label>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <select id="grid-type" name="progress_type" class="form-control cold-md-5 col-xs-12" required>
                                        <option value="percentage">Persentase (%)</option>
                                        <option value="flat">Flat (Rp.)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="grid-progress">Progresi Harga <span class="required">*</span></label>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <input id="grid-progress" type="number" class="form-control col-md-5 col-xs-12" name="price_progress" required>
                                </div>
                            </div>
                            <div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                    <button type="submit" class="btn btn-success">Tambahkan Grid</button>
								</div>
							</div>
						{!! BootForm::close() !!}
					</div>
				</div>
			</div>
		</div>
    </div>
    
@endsection
@push('scripts')
	<script type="text/javascript">
        
	</script>
@endpush