@extends('layouts.blank')

@push('stylesheets')
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css">
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
    	<h3>Grid Customer</h3>
    	<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Grid pelanggan yang tersimpan</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
                        <div id="customers" class="data-journal">
                            {!! $dataTable->table(['id' => 'table_grid', 'class' => 'table dt-responsive display', 'width' => '100%', 'cellspacing' => '0']) !!}
                        </div>
                        <div class="clearfix"></div>
                        @can('edit grid')
						<div style="margin-top:2rem">
                            <a href="{{route('viewAddGrid')}}" id="addNew" class="btn btn-dark"><i class="fa fa-plus"></i> Tambah Grid Baru</a>
                        </div>
                        @endcan
					</div>
				</div>
			</div>
		</div>
    </div>
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">

    </div>
    <!-- /page content -->
@endsection

@push ('scripts')
    <!-- Jquery DataTables Script -->
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap.min.js" type="text/javascript"></script>
    {!! $dataTable->scripts() !!}
    <script>
        
    </script>
@endpush