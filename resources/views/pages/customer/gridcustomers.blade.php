@extends('layouts.blank')

@push('stylesheets')
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css">
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
    	<h3>Customer</h3>
    	<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Pelanggan yg terdaftar</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
                        <div id="customers" class="data-journal">
                            {!! $dataTable->table(['id' => 'table_cust', 'class' => 'table dt-responsive display', 'width' => '100%', 'cellspacing' => '0']) !!}
                        </div>
                        <div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
    </div>
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">

    </div>
    <!-- /page content -->
@endsection

@push ('scripts')
    <!-- Jquery DataTables Script -->
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap.min.js" type="text/javascript"></script>
    {!! $dataTable->scripts() !!}
    <script>
        $('#customers').on('click','.btn-activate',function(e){
            let txt = $(this).html();
            let classIs = (txt=='aktifkan')?'btn-success':'btn-danger';
            $('#modals').html('{!! BootForm::open(["url" => route("toggleActive"), "method" => "post"]) !!}<div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">'+txt+' '+$(this).attr('data-name')+ '</h4></div><div class="modal-body"><input type="hidden" name="current_id" value="'+$(this).attr('data-customer')+'"><p class="text-center">'+txt+' pelanggan?</p></div><div class="modal-footer"><a class="btn btn-default" data-dismiss="modal">Batal</a><button type="submit" class="btn '+classIs+'">'+txt+'</button></div></div></div>{!! BootForm::close() !!}').modal('show');
        });
        $('#customers').on('change','.grid-changer',function(e){
            $('#modals').html('{!! BootForm::open(['url' => route('setGrid'), 'method' => 'post']) !!}<div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Ubah Grid '+$(this).attr('data-name')+'</h4></div><div class="modal-body"><input type="hidden" name="level_id" value="'+$(this).val()+'"><input type="hidden" name="current_id" value="'+$(this).attr('data-customer')+'"><p class="text-center">Ubah Grid pelanggan ke '+$(this).find('option:selected').text()+'</p></div><div class="modal-footer"><a class="btn btn-default" data-dismiss="modal">Batal</a><button type="submit" class="btn btn-success">Ubah Grid</button></div></div></div>{!! BootForm::close() !!}').modal('show');
        })
    </script>
@endpush