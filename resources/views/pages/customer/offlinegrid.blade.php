@extends('layouts.blank')

@push('stylesheets')
	
@endpush

@section('main_container')

    <div class="right_col" role="main">
        <h3>Offline Grid</h3>
        <div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Grid Untuk yang transaksi offline (di pasar)</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
                        {!! BootForm::open(['id' =>'offlineGrid', 'url' => route('updateOfflineGrid'), 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="grid-type">Grid Offline <span class="required">*</span></label>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <select id="grid-type" name="offline_grid" class="form-control cold-md-5 col-xs-12" required>
                                        @foreach (\App\Models\Level::all() as $key => $grid)
                                        <option value="{{$grid->id}}" {{($selected==$grid->id)?'selected':''}}>{{$grid->title}}: {{$grid->price_progress}} {{($grid->progress_type=='flat')?' (Rp.)':'(%)'}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                    <button type="submit" class="btn btn-success">Update Grid</button>
								</div>
							</div>
						{!! BootForm::close() !!}
					</div>
				</div>
			</div>
		</div>
    </div>
    
@endsection
@push('scripts')
	<script type="text/javascript">
        
	</script>
@endpush