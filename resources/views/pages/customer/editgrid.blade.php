@extends('layouts.blank')

@push('stylesheets')
	
@endpush

@section('main_container')

    <div class="right_col" role="main">
        <h3>Edit Grid</h3>
        <div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Edit/ubah Data Grid Pelanggan</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
                        {!! BootForm::open(['id' =>'editGrid', 'url' => route('editGrid'), 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                            <input type="hidden" name="current_id" value="{{$grid->id}}">
							<div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="grid-name">Nama Grid <span class="required">*</span></label>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <input id="grid-name" type="text" class="form-control col-md-5 col-xs-12" name="title" value="{{$grid->title}}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="grid-type">Progresi <span class="required">*</span></label>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <select id="grid-type" name="progress_type" class="form-control cold-md-5 col-xs-12" required>
                                        <option value="percentage" {{($grid->progress_type=='percentage')?'selected':''}}>Persentase (%)</option>
                                        <option value="flat" {{($grid->progress_type=='flat')?'selected':''}}>Flat (Rp.)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="grid-progress">Progresi Harga <span class="required">*</span></label>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                    <input id="grid-progress" type="text" class="form-control col-md-5 col-xs-12" name="price_progress" value="{{$grid->price_progress}}" required>
                                </div>
                            </div>
                            <div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                    @can('edit grid')
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                    <button type="button" class="btn btn-delete btn-danger">Hapus Grid</button>
                                    @endcan
								</div>
							</div>
						{!! BootForm::close() !!}
					</div>
				</div>
			</div>
		</div>
    </div>
    @if (\Auth::user()->hasPermissionTo('edit grid'))
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">
		{!! BootForm::open(['url' => route('deleteGrid'), 'method' => 'post']) !!}
			<div class="modal-dialog modal-sm">
    			<div class="modal-content">
	    			<div class="modal-header">
	    				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	    					<span aria-hidden="true">×</span>
	    				</button>
	    				<h4 class="modal-title">Hapus Grid</h4>
	    			</div>
	    			<div class="modal-body">
	    				<input type="hidden" name="current_id" value="{{$grid->id}}">
	    				<p class="text-center">Apa Anda yakin?</p>
	    			</div>
	    			<div class="modal-footer">
	    				<a class="btn btn-default" data-dismiss="modal">Batal</a>
	    				<button type="submit" class="btn btn-danger">Hapus Grid</button>
	    			</div>
	    		</div>
	    	</div>
	    {!! BootForm::close() !!}
    </div>
    @endif
@endsection
@push('scripts')
	<script type="text/javascript">
        $('.btn-delete').click(function(e){
            $('#modals').modal('show');
        })
	</script>
@endpush