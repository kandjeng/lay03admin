@extends('layouts.blank')

@push('stylesheets')
    
@endpush

@section('main_container')
	<style type="text/css">
		meter {	margin: 0 auto 1em;width: 100%;height: .5em;background: none;background-color: rgba(0,0,0,0.1);}
		meter::-webkit-meter-bar {background: none;background-color: rgba(0,0,0,0.1);}

		meter[value="1"]::-webkit-meter-optimum-value { background: #e6abab; }
		meter[value="2"]::-webkit-meter-optimum-value { background: #f5eba3; }
		meter[value="3"]::-webkit-meter-optimum-value { background: #f5c1a3; }
		meter[value="4"]::-webkit-meter-optimum-value { background: #99de99; }

		meter[value="1"]::-moz-meter-bar { background: #e6abab; }
		meter[value="2"]::-moz-meter-bar { background: #f5eba3; }
		meter[value="3"]::-moz-meter-bar { background: #f5c1a3; }
		meter[value="4"]::-moz-meter-bar { background: #99de99; }
		.passtext{display: none;}
		#pass-text h4,#confirm-text h4{position: absolute;top: 0;right: 18px;color: #ccc;font-style: italic;}
		#confirm-text h4{color: #fff}
	</style>
    <!-- page content -->
    <div class="right_col" role="main">
    	<h3>Change Your Login Password</h3>
    	<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Fill your credentials</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
						<form id="changepassform" data-parsley-validate="" class="form-horizontal form-label-left" method="POST" action="/changepass">
							{{ csrf_field() }}
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="current-password">Current Password <span class="required">*</span></label>
								<div class="col-md-5 col-sm-6 col-xs-12">
									<input id="current-password" type="password" class="form-control col-md-5 col-xs-12" name="current-password" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="new-password">New Password <span class="required">*</span></label>
								<div class="col-md-5 col-sm-6 col-xs-12">
									<input id="new-password" type="password" class="form-control col-md-5 col-xs-12" name="new-password" required>
 									<meter max="4" id="pass-meter"></meter>
 									<div id="pass-text">
										<h4></h4>
										<span></span>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								<label for="new-password-confirm" class="control-label col-md-3 col-sm-3 col-xs-12">Ketik ulang password<span class="required">*</span></label>
								<div class="col-md-5 col-sm-6 col-xs-12">
									<input id="new-password-confirm" type="password" class="form-control col-md-5 col-xs-12" name="new-password_confirmation" required>
									<div id="confirm-text">
										<h4></h4>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
									<button class="btn btn-primary" type="reset">Reset</button>
									<button type="submit" class="btn btn-success">Ubah Password</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
    </div>
    <!-- /page content -->
@endsection
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/4.2.0/zxcvbn.js"></script>
    <script type="text/javascript">
    	var strength = {0: "Sangat Lemah",1: "Lemah",2: "Biasa",3: "Kuat",4: "Sangat Kuat"};
    	var pass = document.getElementById('new-password');
    	var confirm = document.getElementById('new-password-confirm');
    	var meter = document.getElementById('pass-meter');
    	var text = document.getElementById('pass-text');
    	var texthead = text.getElementsByTagName("h4")[0];
    	var textcontent = text.getElementsByTagName('span')[0];
    	var confirmtext = document.getElementById('confirm-text').getElementsByTagName('h4')[0];
    	pass.addEventListener('input', function(event){
			var val = pass.value;
			var result = zxcvbn(val);
			meter.value = result.score;
			text.style.display='block';
			if(val !== "") {
				texthead.innerHTML = strength[result.score];
				textcontent.innerHTML = result.feedback.warning + " " + result.feedback.suggestions;
				}
			else {
				texthead.innerHTML = "";
				textcontent.innerHTML = "";
				}
			});
    	confirm.addEventListener('input',function(event){
    		if (pass.value!=confirm.value){
    			confirmtext.innerHTML='Password Beda!';
    			confirm.style.background='#e6abab';
    		}
    		else{
    			confirmtext.innerHTML='';
    			confirm.style.background='#99de99';
    		}
    	})
    </script>
@endpush