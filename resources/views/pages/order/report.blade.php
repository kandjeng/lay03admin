@extends('layouts.blank')

@push('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="{{asset('vendor/daterangepicker/daterangepicker.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="{{asset('vendor/daterangepicker/custom.min.css')}}" />

@endpush

@section('main_container')

    <div class="right_col" role="main">
		<div class="preloader"><i class="fa fa-power-off"></i></div>
        <h3>Laporan Order</h3>
        <div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Rekap Penjualan</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
						<div class="well" style="padding-top:5px;padding-bottom:5px">
                            <div class="row">
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                    <h4>Filter Laporan</h4>
                                </div>
                                <div class="col-md-9 col-sm-8 col-xs-12">
                                    <select name="report_type" class="select-change" id="report-type">
                                        <option value="omzet">Omzet</option>
                                        <option value="lunas">Order Lunas</option>
                                        <option value="hutang">Order Hutang</option>
                                    </select>
                                    <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                        <span></span> <b class="caret"></b>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="reporttable">
                            <table id="table-report" class="table dt-responsive display dataTable dtr-inline">
                                
                            </table>
                        </div>
					</div>
				</div>
			</div>
		</div>
    </div>
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Hapus Admin</h4>
				</div>
				<div class="modal-body">
					
				</div>
			</div>
		</div>
	</div>
@endsection
@push('scripts')
	<!-- Jquery DataTables Script -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="{{asset('vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    
    <script>
        $(document).ready(function(){
            init_daterangepicker();
        });
        function init_daterangepicker() {
            if ("undefined" != typeof $.fn.daterangepicker) {
                console.log("init_daterangepicker");
                var a = function(a, b, c) {
                        console.log(a.toISOString(), b.toISOString(), c), $("#reportrange span").html(a.format("MMMM D, YYYY") + " - " + b.format("MMMM D, YYYY"))
                    },
                    b = {
                        startDate: moment().subtract(29, "days"),
                        endDate: moment(),
                        dateLimit: {
                            days: 60
                        },
                        showDropdowns: !0,
                        showWeekNumbers: !0,
                        timePicker: !1,
                        timePickerIncrement: 1,
                        timePicker12Hour: !0,
                        ranges: {
                            "Hari ini": [moment(), moment()],
                            Kemarin: [moment().subtract(1, "days"), moment().subtract(1, "days")],
                            "7 Hari terakhir": [moment().subtract(6, "days"), moment()],
                            "30 hari lalu": [moment().subtract(29, "days"), moment()],
                            "Bulan ini": [moment().startOf("month"), moment().endOf("month")],
                            "Bulan Kemarin": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
                        },
                        opens: "left",
                        buttonClasses: ["btn btn-default"],
                        applyClass: "btn-small btn-primary",
                        cancelClass: "btn-small",
                        format: "MM/DD/YYYY",
                        separator: " to ",
                        locale: {
                            applyLabel: "Submit",
                            cancelLabel: "Clear",
                            fromLabel: "From",
                            toLabel: "To",
                            customRangeLabel: "Custom",
                            daysOfWeek: ["Ahd", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"],
                            monthNames: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
                            firstDay: 1
                        }
                    };
                $("#reportrange span").html(moment().subtract(29, "days").format("D MMMM, YYYY") + " - " + moment().format("D MMMM, YYYY")), $("#reportrange").daterangepicker(b, a), $("#reportrange").on("show.daterangepicker", function() {
                    //console.log("show event fired")
                }), $("#reportrange").on("hide.daterangepicker", function() {
                    //console.log("hide event fired")
                }), $("#reportrange").on("apply.daterangepicker", function(a, b) {
                    var type = $('select[name="report_type"]').val();
                    console.log(type+ " report");
                    //console.log("apply event fired, start/end dates are " + b.startDate.utc() + " to " + b.endDate.utc());
                    if (type=='omzet'){
                        getOrdersDone(b.startDate.format("YYYY-MM-DD"),b.endDate.format("YYYY-MM-DD"),0);
                    }
                    if (type=='lunas'){
                        getOrdersDone(b.startDate.format("YYYY-MM-DD"),b.endDate.format("YYYY-MM-DD"),6);
                    }
                    if (type=='hutang'){
                        getOrdersDone(b.startDate.format("YYYY-MM-DD"),b.endDate.format("YYYY-MM-DD"),7);
                    }
                }), $("#reportrange").on("cancel.daterangepicker", function(a, b) {
                    //console.log("cancel event fired")
                }), $("#options1").click(function() {
                    $("#reportrange").data("daterangepicker").setOptions(b, a)
                }), $("#options2").click(function() {
                    $("#reportrange").data("daterangepicker").setOptions(optionSet2, a)
                }), $("#destroy").click(function() {
                    $("#reportrange").data("daterangepicker").remove()
                })
            }
        }
        function getOrdersDone(start, end,type){
            
            $('#table-report').DataTable({
                processing: true,
                serverSide: true,
                destroy:true,
                dom : 'Bfrtil',
                lengthMenu: [[-1],['all']],
                buttons : [
                    {extend: 'excel',footer: true},
                    {extend: 'pdf',footer: true},
                    {extend: 'print',footer: true}
                ],
                ajax: {
                    url: '{{route("ordersDone")}}',
                    data: {
                        start:start,
                        end:end,
                        type:type
                    }
                },
                columns: [
                    {data: 'id', name: 'id', title:'order Id'},
                    {data: 'created_at', name: 'created_at',title:'Tgl Order'},
                    {data: 'payment', name: 'payment',title:'Pembayaran',className:'upper'},
                    {data: 'consignee', name: 'consignee',title:'Consignee'},
                    {data: 'basetotal', name: 'basetotal',title: 'Harga dasar'},
                    {data: 'subtotal', name: 'subtotal',title:'SubTotal',searchable: false},
                    {data: 'shipping', name: 'shipping', title:'Ongkir'},
                    {data: 'total', name: 'total',title:'Total Order',searchable: false},
                ],
                footerCallback: function ( row, data, start, end, display ) {
                    var api = this.api(), data;
         
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[^\d]/g,'')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
                    var toMoney =function (num){
                        return 'Rp. '+num.toString().replace(/(\d)(?=(\d{3})+$)/g, "$1.");
                    };
                    baseTotal = api
                        .column( 4, { page: 'current'} ).data().reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
                    subTotal = api
                        .column( 5, { page: 'current'} ).data().reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
                    ongkir = api
                        .column( 6, { page: 'current'} ).data().reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
                    total = api
                        .column( 7, { page: 'current'} ).data().reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
                    $('tfoot').remove();
                    $(this).append('<tfoot><tr><td colspan="4">Total</td><td>'+toMoney(baseTotal)+'</td><td>'+toMoney(subTotal)+'</td><td>'+toMoney(ongkir)+'</td><td>'+toMoney(total)+'</td></tr></tfoot>');
                     //console.log(pageTotal);
                }
            });
            console.log(start+' - '+end);
        }
    </script>
@endpush