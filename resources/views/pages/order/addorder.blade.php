@extends('layouts.blank')

@push('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css">
@endpush

@section('main_container')

    <div class="right_col" role="main">
		<div class="preloader"><i class="fa fa-power-off"></i></div>
        <h3>Order offline</h3>
        <div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Tambah order secara manual</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
						{!! BootForm::open(['id' =>'addorder', 'url' => route('addOrder'), 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                            <div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="order-nama">Nama Customer</label>
								<div class="col-md-8 col-sm-9 col-xs-12">
                                    <input id="order-nama" type="text" class="form-control col-md-5 col-xs-12" name="consignee" required>
								</div>
							</div>
							<div id="productgroup" class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prod-group">Order item <span class="required">*</span></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<div class="row-products"></div>
									<button id="btn-addprod" data-num="0" type="button" class="btn btn-info"><i class="fa fa-plus"></i> tambah produk</button>	
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="order-shipping"></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<h5 id="subtotal" style="font-weight:600;text-align:right;">subtotal Rp. <span>0</span></h5>
								</div>
							</div>
                            <div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
									<button type="submit" class="btn btn-success">Simpan</button>
								</div>
							</div>
						{!! BootForm::close() !!}
					</div>
				</div>
			</div>
		</div>
    </div>
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Tambahkan produk ke order</h4>
				</div>
				<div class="modal-body">
					{!! $dataTable->table(['id'=>'product-table','class' => 'table dt-responsive display', 'width' => '100%', 'cellspacing' => '0'],true) !!}
				</div>
			</div>
		</div>
	</div>
@endsection
@push('scripts')
	<!-- Jquery DataTables Script -->
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap.min.js" type="text/javascript"></script>
    {!! $dataTable->scripts() !!}
	<script type="text/javascript">
		$(document).ready(function(){
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
		});
		$('#btn-addprod').click(function(){
			var isFilled=0;
			var idx= $(this).attr('data-num')-1;
			$('.row-product-'+ idx +' .row input.mustfill').each(function(){
				isFilled += parseInt($(this).val());
			});
			if (isFilled>0 || idx<0){
				$('#modals').modal('show');
			}
		})
		$('#product-table').on('click','.btn-addproduct',function(){
			var idx = $('#btn-addprod').attr('data-num');
			if (parseInt(idx)>0){
				$('button.btn-remprod').remove();
			}
			var id = $(this).attr('data-id');
			var name = $(this).attr('data-name');
			var stock = JSON.parse($(this).attr('data-stock'));
			var price = $(this).attr('data-price');
			var tempStock = '';
			$.each(stock,function(key,item){
				tempStock += "<div class='col-sm-7 col-xs-7'><input type='text' class='form-control' value='"+item['variance']+" (stok: "+(item['stock']-item['keep'])+")' disabled></div><input type='hidden' name='detail["+idx+"]["+key+"][variance]' value='"+item['variance']+"'><div class='col-sm-3 col-xs-3'><input type='number' max='"+(item['stock']-item['keep'])+"' class='form-control mustfill' data-price='"+price+"' name='detail["+idx+"]["+key+"][count]' value='0''></div>"
			});
			var template = "<div class='row row-product-"+idx+"' style='margin-bottom:1rem;'><div class='col-md-6 col-sm-6 col-xs-12'><input type='text' class='form-control col-md-5 col-xs-12' value='"+name+' - Rp. '+price+"' disabled><input type='hidden' name='products["+idx+"]' value='"+id+"'></div><div class='col-sm-6 col-xs-12 row'>";
			$('#productgroup .row-products').append(template+tempStock+"<div class='col-sm-2 col-xs-2 btn-container'><button type='button' class='btn btn-sm btn-danger btn-remprod'><i class='fa fa-minus'></i></button></div></div></div>");
			$('#modals').modal('hide');
			$('#btn-addprod').attr('data-num', parseInt(idx)+1);
		})
		$('#productgroup').on('click','.btn-remprod',function(){
			var idx = $('#btn-addprod').attr('data-num');
			idx-=1;
			$('.row-product-'+idx).remove();
			$('.row-product-'+(idx-1)+' .btn-container').append("<button type='button' class='btn btn-sm btn-danger btn-remprod'><i class='fa fa-minus'></i></button>")
			$('#btn-addprod').attr('data-num', idx);

		})
		$('#productgroup .row-products').on('change','input.mustfill',function(e){
			console.log($(this).val());
			total = 0;
			$('input.mustfill').each(function(idx,data){
				total += ($(this).val() * $(this).attr('data-price'));
			});
			$('#subtotal span').html(total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."))
		})
	</script>
@endpush