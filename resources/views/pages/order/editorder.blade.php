@extends('layouts.blank')

@push('stylesheets')
	
@endpush

@section('main_container')

    <div class="right_col" role="main">
		<div class="preloader"><i class="fa fa-power-off"></i></div>
        <h3>Edit Order</h3>
        <div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Edit Status dan detail order</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
						{!! BootForm::open(['id' =>'editorder', 'url' => route('editOrder'), 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                            <input type="hidden" name="current_id" value="{{$order->id}}">
                            <div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="order-nama">Nama Customer</label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<input id="order-nama" type="text" class="form-control col-md-5 col-xs-12" value="{{$order->consignee}} {{($order->user->is_admin)?'(Offline)':''}}" disabled>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="order-status">Status Order <span class="required">*</span></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
                                    <select id="order-status" class="form-control cold-md-5 col-xs-12" name="status" required>
                                        @foreach (\DB::table('order_statuses')->where('identity','status')->get() as $stat)
                                            <option value="{{$stat->id}}" {{($stat->id==$order->status)?'selected':''}}>
                                                {!!$stat->status!!}
                                            </option>
                                        @endforeach
									</select>
                                </div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="order-payment">Status Pembayaran <span class="required">*</span></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<input class="form-control" type="text" value="{{$order->getPaymentStatus()}}" disabled>
                                </div>
							</div>
							<div id="productgroup" class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="prod-group">Order item <span class="required">*</span></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									@foreach ($order->products as $key=>$product)
									<div id="product-{{$key}}" class="row" style="margin-bottom:1rem;">
										<div class="col-md-5 col-sm-5 col-xs-12">
											<input type="text" class="form-control col-md-5 col-xs-12" value="{{$product->sku}} - {{$product->title}} " disabled>
										</div>
										<div class="col-sm-5 col-xs-10 row">
											@foreach (json_decode($product->pivot->detail) as $det)
											<div class="col-sm-6 col-xs-6">
												<input type="text" class="form-control" value="{{$det->variance}}" disabled>
											</div>
											<div class="col-sm-6 col-xs-6">
												<input type="text" class="form-control" value="{{$det->count}}" disabled>
											</div>
											@endforeach
										</div>
										<div class="col-md-2 col-sm-2 col-xs-2">
											<button class="btn btn-delproduct btn-danger" data-id="{{$product->id}}" data-product="{{$key}}" type="button"><i class="fa fa-trash"></i></button>
										</div>
									</div>
									@endforeach
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="order-shipping"></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<h5 style="font-weight:600;text-align:right;">subtotal Rp. {{number_format($order->getTotal(),0,'','.')}} ({{$order->getCount()}} item)</h5>
								</div>
							</div>
							<div class="form-group">
									<label class="control-label col-md-2 col-sm-2 col-xs-12" for="order-shipping">Berat Total </label>
									<div class="col-md-8 col-sm-9 col-xs-12">
										<input id="order-shipping" type="number" class="form-control col-md-5 col-xs-12" name="shipping" value="{{$order->getTotalWeight()}}" disabled>
									</div>
								</div>
							<div class="form-group">
									<label class="control-label col-md-2 col-sm-2 col-xs-12" for="order-shipping">Ongkos kirim </label>
									<div class="col-md-8 col-sm-9 col-xs-12">
										<input id="order-shipping" type="number" class="form-control col-md-5 col-xs-12" name="shipping" value="{{$order->shipping}}">
									</div>
								</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="order-shipping">Resi Pengiriman</label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<input id="order-shipping" type="number" class="form-control col-md-5 col-xs-12" name="tracking">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="order-shipping"></label>
								<div class="col-md-8 col-sm-9 col-xs-12">
									<h5 style="font-weight:600;text-align:right;">Total Rp. {{number_format($order->getTotal()+$order->shipping,0,'','.')}}</h5>
								</div>
							</div>
                            <div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
									<button type="submit" class="btn btn-success">Simpan</button>
									<button type="button" class="btn btn-order-delete btn-danger">Hapus Order</button>
									<button type="button" class="btn btn-invoice btn-info">Buat Invoice</button>
								</div>
							</div>
						{!! BootForm::close() !!}
					</div>
				</div>
			</div>
		</div>
    </div>
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">

	</div>
@endsection
@push('scripts')
	<script type="text/javascript">
		$('.btn-order-delete').click(function(){
			$('#modals').html('{!! BootForm::open(['url' => route('deleteOrder'), 'method' => 'post']) !!}<div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Hapus Order</h4></div><div class="modal-body"><input type="hidden" name="current_id" value="{{$order->id}}"><p class="text-center">Apa Anda yakin ingin menghapus order ini ?</p></div><div class="modal-footer"><a class="btn btn-default" data-dismiss="modal">Batal</a><button type="submit" class="btn btn-danger">Hapus Order</button></div></div></div>{!! BootForm::close() !!}').modal('show');
		})
		$('.btn-invoice').click(function(){
			$('#modals').html('{!! BootForm::open(['url' => route('addInvoice'), 'method' => 'post']) !!}<div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Buat Invoice</h4></div><div class="modal-body"><input type="hidden" name="current_id" value="{{$order->id}}"><p class="text-center">Buat Invoice untuk order?</p></div><div class="modal-footer"><a class="btn btn-default" data-dismiss="modal">Batal</a><button type="submit" class="btn btn-info">Buat Invoice</button></div></div></div>{!! BootForm::close() !!}').modal('show');
		})
		$('.btn-delproduct').click(function(){
			$('#product-'+$(this).attr('data-product')).remove();
			$('#productgroup').append('<input type="hidden" name="delprod[]" value="'+ $(this).attr('data-id') +'">');
		})
	</script>
@endpush

