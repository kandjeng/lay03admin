@extends('layouts.blank')

@push('stylesheets')
	
@endpush

@section('main_container')

    <div class="right_col" role="main">
        <h3>Invoice</h3>
        <div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Invoice Order</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
                        <br>
                        {!! BootForm::open(['id' =>'addcategory', 'url' => route('editInvoice'), 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                            <input type="hidden" name="current_id" value="{{$order->id}}">
                            <div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12">Action</label>
								<div class="col-lg-7 col-md-8 col-sm-8 col-xs-12">
                                    <button type="button" class="btn btn-inv btn-info btn-preview"><i class="fa fa-eye"></i> preview invoice</button>
                                    <button type="button" class="btn btn-inv btn-info btn-print"><i class="fa fa-print"></i> print invoice</button>
                                    <a href="{{action('InvoiceController@getPDF',$invoice)}}" class="btn btn-inv btn-info btn-download"><i class="fa fa-save"></i> download invoice</a>
								</div>
                            </div>
                            <div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="inv_numb">Nomor Invoice </label>
								<div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="inv_numb" type="text" class="form-control cold-md-5 col-xs-12" value="{{$invoice->invoice_number}}" disabled></input>
                                </div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="user-name">Nama Penerima</label>
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
									<input id="user-name" type="text" class="form-control col-md-5 col-xs-12" disabled value="{{$invoice->order->consignee}}">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="order-status">Status Payment </label>
								<div class="col-md-6 col-sm-6 col-xs-12">
                                    @if(auth()->user()->can('set payment'))
                                    <select id="order-status" class="form-control cold-md-5 col-xs-12" name="payment" required>
                                        @foreach (\DB::table('order_statuses')->where('identity','payment')->get() as $stat)
                                            <option value="{{$stat->id}}" {{($stat->id==$order->payment)?'selected':''}}>
                                                {!!$stat->status!!}
                                            </option>
                                        @endforeach
                                    </select>
                                    @else
                                    <select id="order-status" class="form-control cold-md-5 col-xs-12" name="payment" disabled>
                                        @foreach (\DB::table('order_statuses')->where('identity','payment')->get() as $stat)
                                            <option value="{{$stat->id}}" {{($stat->id==$order->payment)?'selected':''}}>
                                                {!!$stat->status!!}
                                            </option>
                                        @endforeach
                                    </select>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="create_date">Tgl Invoice </label>
								<div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="create_date" type="text" class="form-control cold-md-5 col-xs-12" value="{{$invoice->created_at->format('d/m/Y')}}" disabled></input>
                                </div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="pay_date">Tgl Pelunasan </label>
								<div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="pay_date" type="text" class="form-control cold-md-5 col-xs-12" value="{{(($invoice->payment_date)?$invoice->payment_date:'BELUM BAYAR')}}" disabled></input>
                                </div>
							</div>
							<div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                @if(auth()->user()->can('set payment'))
                                    <button type="submit" class="btn btn-success">Update Invoice</button>
                                @else
                                    <a href="{{route('invoices')}}" class="btn btn-info">Kembali ke daftar invoice</a>
                                @endif    
								</div>
							</div>
						{!! BootForm::close() !!}
					</div>
				</div>
			</div>
		</div>
    </div>
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Invoice</h4>
                </div>
                <div class="modal-body frame-wrap">
                    <iframe id="invoiceGen" title="Generated Invoice" src="{{route('invoice',['invoice'=>$invoice])}}"></iframe>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-default" data-dismiss="modal">Batal</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
	<script type="text/javascript">
		$('.btn-preview').click(function() {
			$('#modals').modal('show');
        });
        $('.btn-print').click(function() {
			$('#invoiceGen').get(0).contentWindow.print();;
        });
	</script>
@endpush