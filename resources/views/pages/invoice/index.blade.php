@extends('layouts.blank')

@push('stylesheets')
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css">
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
    	<h3>Invoice</h3>
    	<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Daftar Invoice dari Order</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br>
                        <div id="invoices" class="data-journal">
                            {!! $dataTable->table(['id'=>'invoice-table','class' => 'table dt-responsive display', 'width' => '100%', 'cellspacing' => '0'],true) !!}
                        </div>
                        <div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
    </div>
    <div id="modals" class="modal fade" tabindex="-1" role="dialog">
    	
    </div>
    <!-- /page content -->
@endsection

@push ('scripts')
    <!-- Jquery DataTables Script -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap.min.js" type="text/javascript"></script>
    
    {!! $dataTable->scripts() !!}
    <script>
        $('#invoice-table').on('change','.payment-changer',function(e){
            var tb = $(this);
            $('#modals').html('{!! BootForm::open(['url' => route('setPayment'), 'method' => 'post']) !!}<div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Ubah Status Pembayaran </h4></div><div class="modal-body"><input type="hidden" name="payment" value="'+$(this).val()+'"><input type="hidden" name="current_id" value="'+$(this).attr('data-order')+'"><p class="text-center">Ubah status pembayaran ke '+$(this).find('option:selected').text()+'</p></div><div class="modal-footer"><a class="btn btn-default" data-dismiss="modal">Batal</a><button type="submit" class="btn btn-success">Ubah Status</button></div></div></div>{!! BootForm::close() !!}').modal('show').on('hidden.bs.modal',function(){
                tb.val(tb.attr('data-default'));
            });
        })
    </script>
@endpush