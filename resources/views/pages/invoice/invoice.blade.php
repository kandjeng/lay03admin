<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Invoice Luck Mas</title>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,700" rel="stylesheet">
    <style>
         @media print {
            @page {
            size:  24.2cm 14cm;
            orientation: portrait;
            }

            body{
            font-family: sans-serif;
            font-size:6pt;
            }

            /* h1,h2,h3{margin:0;} */
            thead { display: table-header-group }
            tfoot { display: table-row-group }
            tr { page-break-inside: avoid }
            table{
            width:100%;
            border-spacing: 0;
            border-color: black;
            }
            header {
    
                    display: grid;
                    grid-template-columns: 1fr 1fr 1fr;
                    margin-bottom:10px;
    
            }
    
            header .luckmas{
    
                text-align: center;
            }
            header .invoice h3{
                text-align: right;
            }
            header .invoice{
            text-align:right;
            }
            main .total{
                margin-top:10px;
                float:right;
            }
        }
      @media screen{
        body{
            font-family: Arial, sans-serif; 
            font-family: 'Ubuntu', sans-serif;
            font-size: 12pt;
            width: 24.2cm;  
            height: 14cm; 
        }
        thead { display: table-header-group }
        tfoot { display: table-row-group }
        tr { page-break-inside: avoid }
        table{
          width:100%;
        }
        header {
  
                display: grid;
                grid-template-columns: 1fr 1fr 1fr;
                margin-bottom:10px;
  
        }
  
        header .luckmas{
  
            text-align: center;
        }
        header .invoice h3{
            text-align: center;
        }
        header .invoice{
          text-align:right;
        }
        main .total{
            margin-top:10px;
            float:right;
        }
      }
    </style>
</head>
<body>
    <header>
        <div class="tujuan">
            <h3>Ditujukan kepada: </h3>
            {{$order->consignee}} <br>
            {{($order->user->is_admin)?'':$order->user->customer->address}} <br>
            {{($order->user->is_admin)?'':$order->user->phone}}
        </div>
        <div class="luckmas">
            <h1>Luck Mas Store</h1>
        </div>
        <div class="invoice">
            <h3>INVOICE</h3>
            No.: {{$invoice->invoice_number}} <br>
            Tgl.: {{$invoice->created_at->format('d/m/Y')}} <br>
            Order ID: #{{sprintf("%06d",$order->id)}}
        </div>
    </header>
    <main>
        <div class="main-table">
            <table border="1">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Item Produk</th>
                        <th>Harga</th>
                        <th>Qty</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($order->products as $key => $product)
                    <tr>
                        <td>{{($key+1)}}</td>
                        <td>{{$product->sku}}</h3>{{$product->title}}</td>
                        <td>{{number_format($product->markupPrice($grid),0,',','.')}}</td>
                        <td>@foreach(json_decode($product->pivot->detail,true) as $item)
                                ({{$item['variance'].' : '.$item['count']}}) {!!($loop->last)?'':'<br>'!!}
                              @endforeach</td>
                        <td>Rp {{number_format($product->pivot->qty * $product->markupPrice($grid),0,',','.')}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="total">
            <table>
                <tfoot>
                    <tr>
                        <td>Total Barang: </td>
                        <td>{{$order->getCount()}} items</td>
                    </tr>
                    <tr>
                        <td>Berat Total: </td>
                        <td>{{$order->getTotalWeight()}} kg</td>
                    </tr>
                    <tr>
                        <td>Subtotal: </td>
                        <td>Rp. {{number_format($order->getTotal(),0,',','.')}}</td>
                    </tr>
                    <tr>
                        <td>Ongkos Kirim: </td>
                        <td>Rp. {{number_format($order->shipping,0,',','.')}}</td>
                    </tr>
                    <tr>
                        <td>GRAND TOTAL: </td>
                        <td>Rp. {{number_format($order->getTotal() + $order->shipping,0,',','.')}}</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </main>
</body>
</html>