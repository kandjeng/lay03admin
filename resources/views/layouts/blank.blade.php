<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        
        <title>Luck Mas Admin Panel by Butternut Digital</title>

        <!-- Bootstrap -->
        <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="{{ asset("css/gentelella.min.css") }}" rel="stylesheet">
        <!-- PNotify -->
        <link href="{{ asset('css/pnotify.custom.min.css') }}" rel="stylesheet">
        
        @stack('stylesheets')

    </head>

    <body class="nav-md footer_fixed">
        <div class="container body">
            <div class="main_container">

                @include('includes/sidebar')

                @include('includes/topbar')

                @yield('main_container')

                @include('includes/footer')

            </div>
        </div>

        <!-- jQuery -->
        <script src="{{ asset("js/jquery.min.js") }}"></script>
        <!-- Bootstrap -->
        <script src="{{ asset("js/bootstrap.min.js") }}"></script>
        <!-- Custom Theme Scripts -->
        <script src="{{ asset("js/gentelella.min.js") }}"></script>
        <!-- PNotify -->
        <script src="{{ asset('js/pnotify.custom.min.js') }}"></script>
        
        <script type="text/javascript">
        $(document).ready(function(){
        @if (Session::has('success'))
            new PNotify({
                    title: '{{Session::get('success')}}',
                    text: '{{Session::get('text')}}',
                    type: 'success',
                    styling : 'bootstrap3'
                });
        @elseif (Session::has('warning'))
            new PNotify({
                    title: '{{Session::get('warning')}}',
                    text: '{{Session::get('text')}}',
                    type: 'error',
                    styling : 'bootstrap3'
                });
        @endif
        });
        </script>
        @stack('scripts')

    </body>
</html>