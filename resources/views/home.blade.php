@extends('layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="row top_tiles">
            <div class="animated flipInY col-lg-3 col-md-6 col-sm-6 col-xs-12">
              <div class="tile-stats">
                <div class="icon"><i class="fa fa-child"></i></div>
                <div class="count">{{$user_count}}</div>
                <h3>Total Customer</h3>
                <p>Customer yang terdaftar di aplikasi</p>
              </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-6 col-sm-6 col-xs-12">
              <div class="tile-stats">
                <div class="icon"><i class="fa fa-database"></i></div>
                <div class="count">{{$product_count}}</div>
                <h3>Total Produk</h3>
                <p>Jumlah produk aktif yg dijual</p>
              </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-6 col-sm-6 col-xs-12">
              <div class="tile-stats">
                <div class="icon"><i class="fa fa-book"></i></div>
                <div class="count">{{$order_count}}</div>
                <h3>Total Order</h3>
                <p>Jumlah order yang telah selesai diproses</p>
              </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-6 col-sm-6 col-xs-12">
              <div class="tile-stats">
                <div class="icon"><i class="fa fa-calculator"></i></div>
                <div class="count">{{$order_sum}}</div>
                <h3>Total Transaksi</h3>
                <p>Keseluruhan nilai transaksi</p>
              </div>
            </div>
        </div>
        <div class="row" style="margin-bottom:50px;">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">

                <div class="x_title">
                  <h2>Pembelian Customer <small>data dan ranking customer</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="col-md-9 col-sm-9 col-xs-12">
                  <div id="chart">
                      <canvas id="sale_graph" width="794" height="400"></canvas>
                  </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 bg-white">
                    <div class="x_title">
                        <h2>Top Customer</h2>
                        <div class="clearfix"></div>
                    </div>
                    <ul class="list-unstyled top_profiles scroll-view">
                        @foreach ($top_user as $user)
                        <li class="media event">
                          <a class="pull-left border-aero profile_thumb">
                            <i class="fa fa-user aero"></i>
                          </a>
                          <div class="media-body">
                            <a class="title" href="{{route('reportUser',['id'=>$user->id])}}">{{$user->name}}</a>
                            <p>Total Transaksi <strong>Rp {{money_short($user->orders->sum(function($usr){return $usr->getTotal();}),0,',','.')}}. </strong></p>
                            <p> <small>dari {{$user->orders->count()}} order</small>
                            </p>
                          </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="clearfix"></div>
                
              </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.0/Chart.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.0/Chart.min.js"></script>
    <script>
        $(document).ready(function(){
            var ctx = document.getElementById("sale_graph").getContext("2d");

            var data = {
              labels: [
                  @foreach($prev_order as $key => $item)
                  "{{$key}}",
                  @endforeach
              ],
              datasets: [{
                label: "Order",
                backgroundColor: "#2a3f54",
                yAxisID:'order',
                data: [
                  @foreach ($prev_order as $key => $order)
                  {{$order->count()}},
                  @endforeach
                ]
              }, {
                label: "Total(Rp)",
                backgroundColor: "#6d95bd",
                yAxisID:'total',
                data: [
                @foreach ($prev_order as $key => $order)
                  {{($order->count()?$order->sum(function($ord){return $ord->getTotal();}):'0')}},
                @endforeach
                ]
              }]
            };
            
            var myBarChart = new Chart(ctx, {
              type: 'bar',
              data: data,
              options: {
                barValueSpacing: 20,
                scales: {
                  yAxes: [{
		     id:'order',
		     type:'linear',
		     position:'left',
		     ticks:{
		        fixedStepSize:1
		     }
                  },{
                     id:'total',
                     type: 'linear',
                     position:'right',
                     display:false
                  }]
                }
              }
            });
        })
    </script>
@endpush