<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Luck Mas Admin Panel </title>
    
    <!-- Bootstrap -->
    <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset("css/gentelella.min.css") }}" rel="stylesheet">

</head>

<body class="login">
<div>
    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
				{!! BootForm::open(['url' => url('/login'), 'method' => 'post']) !!}
                    
				<h1><img src="/images/logo.png" width="46px" alt="Luck Mas Logo"> Luck Mas Admin</h1>
			
				{!! BootForm::text('phone', false, old('phone'), ['placeholder' => 'Nomor Handphone', 'afterInput' => '<span>test</span>'] ) !!}
			
				{!! BootForm::password('password', false, ['placeholder' => 'Password']) !!}
				
				<div>
					{!! BootForm::submit('Log in', ['class' => 'btn btn-default submit']) !!}
					<div class="reset_pass">
						<input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
						<label for="remember">
							Ingat Saya? 
						</label>
					</div>
				</div>
                    
				<div class="clearfix"></div>
                    
				<div class="separator">
					
                        
					<div class="clearfix"></div>
					<br />
                        
					<div>
						Luck Mas v1.0 by <a href="http://butternutwebsite.com">Butternut Digital</a>
					</div>
				</div>
				{!! BootForm::close() !!}
            </section>
        </div>
    </div>
</div>
</body>
</html>