<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Password minimal 8 karakter',
    'reset' => 'Password berhasil di-reset',
    // 'sent' => 'We have e-mailed your password reset link!',
    'sent' => 'Kami telah mengirimkan password Anda melalui e-mail. Mohon periksa inbox email Anda.',
    'token' => 'Token reset password salah',
    'user' => "User dengan email tersebut belum terdaftar atau email Anda salah.",

];
